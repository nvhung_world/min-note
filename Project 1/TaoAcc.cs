﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_1
{
    public partial class TaoAcc : Form
    {
        public TaoAcc()
        {
            InitializeComponent();
        }
        String[] acc = new String[5];
        bool C1 = false, C2 = true, C3 = true;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (Form1.Acc.FillAcc(textBox1.Text.ToLower()) | textBox1.Text.Length <6)
                SenMSG("Tài khoản đã tồn tại hoặc quá ngắn",false);
            else
            {
                SenMSG("Tài khoản khả dụng", true);
                acc[0] = textBox1.Text.ToLower();
            }

        }
        
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (textBox5.Text.Length < 6 | textBox5.Text == textBox1.Text)
            {
                SenMSG("Mật khẩu phải dài hơn 6 kí tự và khác tài khoản", false);
                acc[1] = "";
            }
            else
            {
                SenMSG("", false);
                acc[1] = textBox5.Text;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (textBox4.Text != textBox5.Text)
            {
                SenMSG("Xác nhận mật khẩu không khớp", false);
                C1 = false;
            }
            else
            {
                SenMSG("", false);
                C1 = true;
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text.Length < 6 | textBox5.Text == textBox3.Text)
                SenMSG("Mật khẩu cấp 2 phải dài hơn 6 kí tự và khác mật khẩu cấp 1", false);
            else
            {
                SenMSG("", false);
                acc[2] = textBox3.Text;
                C2 = false;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text != textBox3.Text)
            {
                C2 = false;
                SenMSG("Xác nhận mật khẩu cấp 2 không khớp", false);
            }
            else
            {
                SenMSG("", false);
                C2 = true;
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            if (textBox7.Text.Length < 6)
            {
                C3 = false;
                SenMSG("Câu trả lời phải dài hơn 6 kí tự", false);
            }
            else
            {
                SenMSG("", false);
                acc[4] = textBox7.Text;
                C3 = true;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                acc[3] = comboBox1.Text;
                C3 = false;
            }
            else
            {
                acc[3] = "";
                C3 = true;
            }
        }

        public void SenMSG(String s, bool b)
        {
            if (b)
            {
                MSG.Text = s;
                MSG.ForeColor = Color.Blue;
            }
            else
            {
                MSG.Text = s;
                MSG.ForeColor = Color.Red;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (acc[0] != null & acc[1] != null & C1 & C2 & C3)
            {
                if (textBox4.Text != textBox5.Text)
                {
                    SenMSG("Xác nhận mật khẩu không khớp", false);
                    return;
                }
                if (textBox3.Text != textBox2.Text)
                {
                    SenMSG("Xác nhận mật khẩu cấp 2 không khớp", false);
                    return;
                }
                if (Form1.Acc.CreateAcc(acc))
                    this.Close();
                else
                    SenMSG("Không thể lưu vào cơ sở dữ liệu", false);
            }
            else
                SenMSG("Hãy điền đầy đủ thông tin", false);
        }

        private void TaoAcc_Load(object sender, EventArgs e)
        {
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - 475, 30);
        }
    }
}
