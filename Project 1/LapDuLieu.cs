﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
using System.Data.SQLite;
using System.Threading;
namespace Project_1
{
    public partial class LapDuLieu : Form
    {
        public DTB_CongViec CongViec;
        public DTB_NhatKi NhatKi;
        public DTB_Note Note;
        public DTB_Sach Sach;
        DTB_Connect con;
        Form2 F;
        Thread D;
        public LapDuLieu()
        {
            InitializeComponent();
        }
        public LapDuLieu(DTB_Connect con,Form2 F)
        {
            InitializeComponent();
            this.F = F;
            this.con = con;
            D = new Thread(new ThreadStart(Start));
            D.Start();
            timer1.Start();
        }
        public void Start()
        {
            CongViec = new DTB_CongViec(con);
            NhatKi = new DTB_NhatKi(con);
            Note = new DTB_Note(con);
            Sach = new DTB_Sach(con);
        }
        private void LapDuLieu_FormClosed(object sender, FormClosedEventArgs e)
        {
            F.Loat_Finalli();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Sach != null)
            {
                Close();
            }
        }

        private void ccT_Loat1_Load(object sender, EventArgs e)
        {
            Location = new Point(F.Location.X, F.Location.Y + 50);
        }
    }
}
