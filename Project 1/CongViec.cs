﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DTB;
namespace Project_1
{
    public partial class CongViec : Form
    {
        public bool isShow = false;
        public DTB_Connect con;
        DTB_CongViec CV;
        eCuaSo Cs;
        public CongViec()
        {
            InitializeComponent();
            timer1.Start();
        }

        public CongViec(DTB_Connect con, DTB_CongViec Data,eCuaSo Cs)
        {
            this.con = con;
            this.CV = Data;
            this.Cs = Cs;
            InitializeComponent();

            ccT_Folder1.Start(CV.Data);
            timer1.Start();
        }

        private void CongViec_Load(object sender, EventArgs e)
        {
            if (Cs.ViTri_X > -1 & Cs.ViTri_Y > -1)
                Location = new Point(Cs.ViTri_X, Cs.ViTri_Y);
        }
        //..............................
        bool isDown = false;
        DateTime old_Time;
        private void button1_Click(object sender, EventArgs e)
        {
            isDown = !isDown;
            old_Time = DateTime.Now;
            if (isDown)
            {
                button1.BackgroundImage = global::Project_1.Properties.Resources._798df;
            }
            else
            {
                button1.BackgroundImage = global::Project_1.Properties.Resources._31132;
                button1.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            double D = (DateTime.Now - old_Time).TotalSeconds;
            int i = (int)(500 * D);
            if (isDown)
            {
                if (Size.Width < 705)
                {
                    panel2.Location = new Point(panel2.Location.X + i, panel2.Location.Y);
                    this.Size = new Size(Size.Width + i, Size.Height);
                }
                if (Size.Width > 705)
                {
                    panel2.Location = new Point(300, panel2.Location.Y);
                    this.Size = new Size(705, Size.Height);
                }
            }
            else
            {
                if (Size.Width > 410)
                {
                    panel2.Location = new Point(panel2.Location.X - i, panel2.Location.Y);
                    this.Size = new Size(Size.Width - i, Size.Height);
                }
                if (Size.Width < 410)
                {
                    panel2.Location = new Point(5, panel2.Location.Y);
                    this.Size = new Size(410, Size.Height);
                }
            }
            old_Time = DateTime.Now;
        }
        //..................................................
        bool BoolNew;
        eCongViec eItem;
        CCT.CCT_Item UC;
        private void button2_Click(object sender, EventArgs e)
        {
            NewCongviec();
        }
        public void NewCongviec()
        {
            button1.Enabled = true;
            isDown = true;
            old_Time = DateTime.Now;
            button1.BackgroundImage = global::Project_1.Properties.Resources._798df;

            eItem = new eCongViec(con);
            BoolNew = true;
            ccT_XepHang1.Var = 1;
            textBox1.Text = "Tiêu đề";
            richTextBox1.Text = "Nội dung";
            textBox2.Text = DateTime.Now.AddSeconds(0).ToString();
            comboBox1.Text = "";
            textBox3.Text = "";
            checkBox1.Checked = false;
            checkBox2.Checked = false;

            button4.Enabled = true;
        }
        private void ccT_Folder1_OnMoDong(CCT.CCT_Item UC, eData eItem,int index)
        {
            button1.Enabled = true;
            isDown = true;
            button1.BackgroundImage = global::Project_1.Properties.Resources._798df;
            old_Time = DateTime.Now;

            this.eItem = (eCongViec)eItem;
            this.UC = UC;
            BoolNew = false;

            ccT_XepHang1.Var = ((eCongViec)eItem).XepHang;
            textBox1.Text = ((eCongViec)eItem).TieuDe;
            richTextBox1.Text = ((eCongViec)eItem).NoiDung;
            textBox2.Text = ((eCongViec)eItem).DateTime.ToString();
            comboBox1.Text = ((eCongViec)eItem).LapLai;
            textBox3.Text = ((eCongViec)eItem).OpenFile;

            if (((eCongViec)eItem).DongMinNote == "True")
                checkBox1.Checked = true;
            else
                checkBox1.Checked = false;
            if (((eCongViec)eItem).TatMay == "True")
                checkBox2.Checked = true;
            else
                checkBox2.Checked = false;

            button4.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MSG.Text = "Tiêu đề không được bỏ trống !";
                return;
            }
            eItem.XepHang = ccT_XepHang1.Var;
            eItem.TieuDe = textBox1.Text;
            eItem.NoiDung = richTextBox1.Text;
            if (textBox2.Text != "")
            {
                try
                {
                    DateTime dt = Convert.ToDateTime(textBox2.Text);
                    eItem.DateTime = new DateTime(dt.Year,dt.Month,dt.Day,dt.Hour,dt.Minute,0);
                    eItem.LapLai = comboBox1.Text;
                }
                catch (Exception Ex)
                {
                    MSG.Text = "Lỗi định dạng thời gian !";
                    return;
                }
            }
            eItem.OpenFile = textBox3.Text;
            if (checkBox1.Checked)
                eItem.DongMinNote = "True";
            else
                eItem.DongMinNote = "False";

            if (checkBox1.Checked)
                eItem.TatMay = "True";
            else
                eItem.TatMay = "False";

            MSG.Text = "";
            if (BoolNew)
            {
                eItem.Acc = DTB_Acc.Acc;
                eItem.ID = CV.maxID +1 ;
                CV.maxID += 1;
                eItem.LastDay = DateTime.Now;
                eItem.NgayTao = DateTime.Now;
                ccT_Folder1.AddButton(eItem);
            }
            else
            {
                UC.Up_Date();
            }
            button1.Enabled = false;
            isDown = false;
            old_Time = DateTime.Now;
            button1.BackgroundImage = global::Project_1.Properties.Resources._31132;

            button4.Enabled = false;
        }


        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text != "")
            {
                try
                {
                    Convert.ToDateTime(textBox2.Text);
                    MSG.Text = "";
                    comboBox1.Text = comboBox1.Items[0].ToString();
                }
                catch (FormatException Ex)
                {
                    MSG.Text = "Lỗi định dạng thời gian !";
                    comboBox1.Text = "";
                }
            }
        }
        //..................................................
        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void CongViec_LocationChanged(object sender, EventArgs e)
        {
            con.Command("UPDATE ViTriCuaSo Set ViTri_X = '" + MaHoa.Ma_Hoa(Location.X.ToString()) +
                                                        "', ViTri_Y = '" + MaHoa.Ma_Hoa(Location.Y.ToString()) +
                                                        "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                        "' and TenCuaSo = '" + MaHoa.Ma_Hoa("CongViec") + "'");

            Cs.ViTri_X = Location.X;
            Cs.ViTri_Y = Location.Y;
        }



        //.......................................................


        
    }
}
