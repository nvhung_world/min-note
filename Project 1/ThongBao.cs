﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DTB;
using IrrKlang;
namespace Project_1
{
    public partial class ThongBao : Form
    {
        eCongViec eCV;
        int i;
        bool B;
        DateTime D;
        public ThongBao()
        {
            InitializeComponent();
        }
        Form2 F;
        ISoundEngine ISE;
        public ThongBao(eCongViec e, int i, bool B, Form2 F,int volume)
        {
            InitializeComponent();
            this.eCV = e;
            this.i = i;
            this.F = F;
            this.B = B;
            D = DateTime.Now;
            X = Screen.PrimaryScreen.Bounds.Width / 2 - 225;
            Y = Screen.PrimaryScreen.Bounds.Height / 2 - 87;
            textBox1.Hide();
            ISE = new ISoundEngine();
            ISE.Play2D("a.wav",false);
            ISE.SetAllSoundsPaused(false);
        }
        int X, Y;
        private void ThongBao_Load(object sender, EventArgs e)
        {
            
            int i = this.i / 10;
            int j = this.i % 10;
            this.Location = new Point(X + 20 * j, Y + 70 * i);

            if (eCV.Cham_Che)
            {
                this.BackColor = Color.Red;
                this.Text = "Thông báo muộn";
            }

            ccT_XepHang1.Var = eCV.XepHang;
            label1.Text = eCV.TieuDe;
            label2.Text = "Thời gian: " + eCV.DateTime.ToString();
            label3.Text = "Lặp lại: " + eCV.LapLai;

            if (eCV.OpenFile != "")
            {
                label4.Text = "Open File: " + eCV.OpenFile;
                try
                {
                    System.Diagnostics.Process.Start(eCV.OpenFile);
                }
                catch (FileNotFoundException Ex)
                {
                    label4.Text = "Open File: Không tìm thấy file";
                }
            }
            else
                label4.Text = "Open File: Không ";

            if (eCV.DongMinNote == "True")
                label5.Text = "Đăng xuất sau 10 giây";
            else
                label5.Text = "Đăng xuất: Không ";

            if (eCV.TatMay == "True")
                label6.Text = "Tắt máy sau 10 giây";
            else
                label6.Text = "Tắt máy: Không ";
            if (eCV.DongMinNote == "True" | eCV.TatMay == "True")
            {
                button2.Text = "Ngưng lại";
                timer1.Start();
            }
            else
                button2.Text = "Đã xem";

            richTextBox1.Text = eCV.NoiDung;
        }
        //..........................................................
        private void button1_Click(object sender, EventArgs e)
        {
            if (B)
            {
                textBox1.Show();
            }
            else
            {
                this.Size = new Size(550, 320);
            }
        }
        private void textBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                if(textBox1.Text == DTB_Acc.Pass)
                    this.Size = new Size(550, 320);
            }
        }
        //.......................................................
        private void button2_Click(object sender, EventArgs e)
        {
            if (eCV.DongMinNote == "True" | eCV.TatMay == "True")
            {
                timer1.Stop();
            }
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if ((DateTime.Now - D).TotalSeconds > 11)
            {

                if (eCV.TatMay == "True" & eCV.DongMinNote == "True")
                {
                    System.Diagnostics.Process.Start("s­hutdown", "-s -t 1");
                    F.DangXuat();
                    Application.Exit();
                }
                if (eCV.TatMay == "True")
                {
                    System.Diagnostics.Process.Start("s­hutdown", "-s -t 1");
                    Application.Exit();
                }
                if (eCV.DongMinNote == "True")
                {
                    F.DangXuat();
                }
            }
            else
            {
                if (eCV.DongMinNote == "True")
                {
                    int i = 10 - (int)(DateTime.Now - D).TotalSeconds;
                    label5.Text = "Đăng xuất sau " + i.ToString() + " giây";
                }
                if (eCV.TatMay == "True")
                {
                    label6.Text = "Tắt máy sau " + i.ToString() + " giây";
                }
            }
        }

        private void ThongBao_FormClosing(object sender, FormClosingEventArgs e)
        {
            ISE.Dispose();
        }
    }
}
