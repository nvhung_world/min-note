﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
namespace Project_1
{
    public partial class Form1 : Form
    {
        public static DTB_Acc Acc;
        public DTB_CaiDatDangNhap CaiDat;
        public Form1()
        {
            InitializeComponent();
            Acc = new DTB_Acc();
            Acc.Open();
            CaiDat = new DTB_CaiDatDangNhap();
            CaiDat.Open();
            checkBox1.Checked = CaiDat.getGhiNho();
            textBox1.Text = CaiDat.getAcc();
        }
        private void label4_Click(object sender, EventArgs e)
        {
            new TaoAcc().ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            new QuyenPass().ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Acc.Dang_Nhap(textBox1.Text, textBox2.Text))
                MSG.Text = "Sai tài khoản hoặc mật khẩu";
            else
            {
                CaiDat.setAcc(textBox1.Text);
                CaiDat.setPass(textBox2.Text);
                Program.B2 = !Program.B2;
                Program.luong = 2;
                Close();

            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CaiDat.setGhiNho(checkBox1.Checked);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Acc.Close();
            CaiDat.Close();
            Program.B1 = !Program.B1;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - 290, 75);
        }

        private void textBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                if (!Acc.Dang_Nhap(textBox1.Text, textBox2.Text))
                    MSG.Text = "Sai tài khoản hoặc mật khẩu";
                else
                {
                    CaiDat.setAcc(textBox1.Text);
                    CaiDat.setPass(textBox2.Text);
                    Program.B2 = !Program.B2;
                    Program.luong = 2;
                    Close();

                }
            }
        }

        private void textBox2_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                if (!Acc.Dang_Nhap(textBox1.Text, textBox2.Text))
                    MSG.Text = "Sai tài khoản hoặc mật khẩu";
                else
                {
                    CaiDat.setAcc(textBox1.Text);
                    CaiDat.setPass(textBox2.Text);
                    Program.B2 = !Program.B2;
                    Program.luong = 2;
                    Close();

                }
            }
        }
    }
}
