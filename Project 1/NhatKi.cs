﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;

namespace Project_1
{
    public partial class NhatKi : Form
    {
        public bool isShow = false;
        DTB_Connect con;
        DTB_NhatKi NK;
        eCuaSo Cs;
        public NhatKi()
        {
            InitializeComponent();
        }

        public NhatKi(DTB_Connect con,DTB_NhatKi NK, eCuaSo Cs)
        {
            InitializeComponent();
            this.con = con;
            this.NK = NK;
            this.Cs = Cs;

            ccT_Folder1.Start(NK.Data);
            timer1.Start();
        }
        //.................................................
        private void NhatKi_Load(object sender, EventArgs e)
        {
            if (Cs.ViTri_X > -1 & Cs.ViTri_Y > -1)
                Location = new Point(Cs.ViTri_X, Cs.ViTri_Y);
        }

        private void NhatKi_LocationChanged(object sender, EventArgs e)
        {
            con.Command("UPDATE ViTriCuaSo Set ViTri_X = '" + MaHoa.Ma_Hoa(Location.X.ToString()) +
                                                        "', ViTri_Y = '" + MaHoa.Ma_Hoa(Location.Y.ToString()) +
                                                        "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                        "' and TenCuaSo = '" + MaHoa.Ma_Hoa("NhatKi") + "'");
            Cs.ViTri_X = Location.X;
            Cs.ViTri_Y = Location.Y;
        }
        //.................................................
        bool isDown = false;
        DateTime old_Time;
        private void button1_Click(object sender, EventArgs e)
        {
            isDown = !isDown;
            old_Time = DateTime.Now;
            if (isDown)
            {
                button1.BackgroundImage = global::Project_1.Properties.Resources._798df;
            }
            else
            {
                button1.BackgroundImage = global::Project_1.Properties.Resources._31132;
                button1.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            double D = (DateTime.Now - old_Time).TotalSeconds;
            int i = (int)(500 * D);
            if (isDown)
            {
                if (Size.Width < 730)
                {
                    panel1.Location = new Point(panel1.Location.X + i, panel1.Location.Y);
                    this.Size = new Size(Size.Width + i, Size.Height);
                }
                if (Size.Width > 730)
                {
                    panel1.Location = new Point(320, panel1.Location.Y);
                    this.Size = new Size(730, Size.Height);
                }
            }
            else
            {
                if (Size.Width > 420)
                {
                    panel1.Location = new Point(panel1.Location.X - i, panel1.Location.Y);
                    this.Size = new Size(Size.Width - i, Size.Height);
                }
                if (Size.Width < 420)
                {
                    panel1.Location = new Point(10, panel1.Location.Y);
                    this.Size = new Size(420, Size.Height);
                }
            }
            old_Time = DateTime.Now;
        }
        //.................................................

        bool BoolNew;
        eNhatKi eItem;
        DateTime D;
        CCT.CCT_Item UC;
        private void button2_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            isDown = true;
            old_Time = DateTime.Now;
            button1.BackgroundImage = global::Project_1.Properties.Resources._798df;

            BoolNew = true;
            eItem = new eNhatKi(con);
            D = DateTime.Now;
            label1.Text = "Ngày: " + D.ToString();
            textBox1.Text = "";
            richTextBox1.Text = "";

            button3.Enabled = true;

        }
        private void ccT_Folder1_OnMoDong(CCT.CCT_Item UC, eData eItem, int index)
        {
            button1.Enabled = true;
            isDown = true;
            old_Time = DateTime.Now;
            button1.BackgroundImage = global::Project_1.Properties.Resources._798df;

            BoolNew = false;
            this.eItem = (eNhatKi)eItem;
            D = ((eNhatKi)eItem).NgayTao;
            this.UC = UC;

            label1.Text = "Ngày: " + D.ToString();
            textBox1.Text = ((eNhatKi)eItem).CamThay;
            richTextBox1.Text = ((eNhatKi)eItem).NoiDung;

            button3.Enabled = true;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MSG.Text = "Hãy viết cảm súc hiện thời của bạn!";
                return;
            }
            if (richTextBox1.Text == "")
            {
                MSG.Text = "Phần nội dung không được bỏ trống !";
                return;
            }
            eItem.NgayTao = D;
            eItem.CamThay = textBox1.Text;
            eItem.NoiDung = richTextBox1.Text;
            eItem.Acc = DTB_Acc.Acc;

            MSG.Text = "";
            if (BoolNew)
            {
                eItem.ID = NK.maxID;
                NK.maxID += 1;
                ccT_Folder1.AddButton(eItem);
            }
            else
            {
                UC.Up_Date();
            }

            button1.Enabled = false;
            isDown = false;
            old_Time = DateTime.Now;
            button1.BackgroundImage = global::Project_1.Properties.Resources._31132;

            button3.Enabled = false;
        }

        //......................................................
    }
}
