﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
using System.Collections;
namespace Project_1
{
    public partial class Sach : Form
    {
        public bool isShow = false;
        public DTB_Connect con;
        public DTB_Sach S;
        public eCuaSo Cs;
        public Sach()
        {
            InitializeComponent();
            timer1.Start();
            button1.Hide();
            textBox1.Hide();
        }
        public Sach(DTB_Connect con, DTB_Sach S, eCuaSo Cs)
        {
            InitializeComponent();
            this.con = con;
            this.S = S;
            this.Cs = Cs;

            timer1.Start();
            ccT_Folder1.Start(S.Data);
            button1.Hide();
            textBox1.Hide();
            button4.Hide();
        }
        private void Sach_Load(object sender, EventArgs e)
        {
            if (Cs.ViTri_X > -1 & Cs.ViTri_Y > -1)
                Location = new Point(Cs.ViTri_X, Cs.ViTri_Y);
        }

        //......................................................
        bool isDown = false;
        DateTime old_Time;
        private void timer1_Tick(object sender, EventArgs e)
        {
            double D = (DateTime.Now - old_Time).TotalSeconds;
            int i = (int)(500 * D);
            if (isDown)
            {
                if (Size.Width < 700)
                {
                    panel1.Location = new Point(panel1.Location.X + i, panel1.Location.Y);
                    this.Size = new Size(Size.Width + i, Size.Height);
                }
                if (Size.Width > 700)
                {
                    panel1.Location = new Point(300, panel1.Location.Y);
                    this.Size = new Size(700, Size.Height);
                }
            }
            else
            {
                if (Size.Width > 405)
                {
                    panel1.Location = new Point(panel1.Location.X - i, panel1.Location.Y);
                    this.Size = new Size(Size.Width - i, Size.Height);
                }
                if (Size.Width < 405)
                {
                    panel1.Location = new Point(5, panel1.Location.Y);
                    this.Size = new Size(405, Size.Height);
                }
            }
            old_Time = DateTime.Now;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            isDown = !isDown;
            old_Time = DateTime.Now;
            if (isDown)
            {
                button2.BackgroundImage = global::Project_1.Properties.Resources._798df;
            }
            else
            {
                button2.BackgroundImage = global::Project_1.Properties.Resources._31132;
                //button2.Enabled = false;
            }
        }

        //.........................................................
        private void Sach_LocationChanged(object sender, EventArgs e)
        {
            con.Command("UPDATE ViTriCuaSo Set ViTri_X = '" + MaHoa.Ma_Hoa(Location.X.ToString()) +
                                                        "', ViTri_Y = '" + MaHoa.Ma_Hoa(Location.Y.ToString()) +
                                                        "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                        "' and TenCuaSo = '" + MaHoa.Ma_Hoa("Sach") + "'");

            Cs.ViTri_X = Location.X;
            Cs.ViTri_Y = Location.Y;
        }
        //...............................................................
        CCT.CCT_Item UC;
        eTuSach eTu;
        eChuong eItem;
        bool BoolNew;
        private void ccT_Folder1_OnMoDong(CCT.CCT_Item UC, eData eItem, int index)
        {
            button2.Enabled = true;
            isDown = true;
            button2.BackgroundImage = global::Project_1.Properties.Resources._798df;
            old_Time = DateTime.Now;

            this.eItem = (eChuong)eItem;
            this.UC = UC;
            BoolNew = false;

            textBox2.Text = this.eItem.TenChuong;
            ccT_XepHang1.Var = this.eItem.XepHang;
            textBox3.Text = this.eItem.TieuDe;
            richTextBox1.Text = this.eItem.NoiDung;

            button5.Enabled = true;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            button2.Enabled = true;
            isDown = true;
            button2.BackgroundImage = global::Project_1.Properties.Resources._798df;
            old_Time = DateTime.Now;

            BoolNew = true;
            eItem = new eChuong(con);

            textBox2.Text = "Tên Chương";
            ccT_XepHang1.Var = 1;
            textBox3.Text = "Tiêu đề";
            richTextBox1.Text = "Nội dung";

            button5.Enabled = true;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                MSG.Text = "Tên Chương không được bỏ trống!";
                return;
            }
            if (textBox3.Text == "")
            {
                MSG.Text = "Tiêu đề không được bỏ trống!";
                return;
            }
            if (richTextBox1.Text == "")
            {
                MSG.Text = "Nội dung không được bỏ trống!";
                return;
            }
            eItem.NoiDung = richTextBox1.Text;
            eItem.TenChuong = textBox2.Text;
            eItem.TieuDe = textBox3.Text;
            eItem.XepHang = ccT_XepHang1.Var;
            
            if (BoolNew)
            {
                eItem.NgayTao = DateTime.Now.ToString();
                eItem.BaseID = eTu.ID;
                eItem.ID = eTu.maxID_Chuong + 1;
                eTu.maxID_Chuong += 1;
                eItem.Acc = DTB_Acc.Acc;
                ccT_Folder1.AddButton(eItem);
            }
            else
            {
                UC.Up_Date();
            }

            button2.Enabled = false;
            isDown = false;
            old_Time = DateTime.Now;
            button2.BackgroundImage = global::Project_1.Properties.Resources._31132;

            button5.Enabled = false;
        }
        //............................................................
        private void ccT_Folder1_OnOpen(CCT.CCT_Tep CT, eTuSach eItem, int index)
        {
            button4.Show();
            button3.Hide();
            button1.Show();
            ccT_Folder1.ClearButton();
            ccT_Folder1.Start(eItem.Data);
            eTu = eItem;
            if (texBox1IsShow)
            {
                texBox1IsShow = false;
                textBox1.Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button4.Hide();
            button3.Show();
            button1.Hide();
            ccT_Folder1.ClearButton();
            ccT_Folder1.Start(S.Data);
        }
        //........................................................
        //......................................................
        //......................................................
        bool texBox1IsShow = false;
        private void button3_Click(object sender, EventArgs e)
        {
            if (!texBox1IsShow)
            {
                texBox1IsShow = true;
                textBox1.Show();
                textBox1.Text = "";
            }
            else
            {
                texBox1IsShow = false;
                textBox1.Hide();
            }
        }

        private void textBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (textBox1.Text != "" & e.KeyData == Keys.Enter)
            {
                eTuSach eTu = new eTuSach(con);
                eTu.Acc = DTB_Acc.Acc;
                eTu.ID = S.maxID_TuSach + 1;
                S.maxID_TuSach += 1;
                eTu.TenSach = textBox1.Text;
                eTu.NgayTao = DateTime.Now;
                eTu.TabData = "Sach";
                eTu.Data = new ArrayList();
                ccT_Folder1.AddTu(eTu);
                textBox1.Hide();
                texBox1IsShow = false;
            }
        }


        //.......................................................
    }
}
