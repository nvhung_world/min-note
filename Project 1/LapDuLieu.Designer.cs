﻿namespace Project_1
{
    partial class LapDuLieu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LapDuLieu));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.ccT_Loat1 = new CCT.CCT_Loat();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Xin đợi !...";
            // 
            // ccT_Loat1
            // 
            this.ccT_Loat1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ccT_Loat1.Location = new System.Drawing.Point(10, 30);
            this.ccT_Loat1.Name = "ccT_Loat1";
            this.ccT_Loat1.Size = new System.Drawing.Size(230, 20);
            this.ccT_Loat1.TabIndex = 0;
            // 
            // LapDuLieu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(230, 60);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ccT_Loat1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LapDuLieu";
            this.ShowInTaskbar = false;
            this.Text = "LapDuLieu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LapDuLieu_FormClosed);
            this.Load += new System.EventHandler(this.ccT_Loat1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CCT.CCT_Loat ccT_Loat1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
    }
}