﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_1
{
    public partial class ComfixPass : Form
    {
        public ComfixPass()
        {
            InitializeComponent();
        }
        public bool isShow;
        public Form F;
        Point P;
        public ComfixPass(Form F,Point P)
        {
            InitializeComponent();
            this.P = P;
            this.F = F;
            i = 0;
        }
        private void ComfixPass_Load(object sender, EventArgs e)
        {
            this.Location = P;
        }
        DateTime old_Time;
        int i ;
        private void textBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox1.Text == DTB.DTB_Acc.Pass)
                {
                    i = 0;
                    textBox1.Text = "";
                    F.Show();

                    if (F is CongViec)
                        ((CongViec)F).isShow = true;
                    if (F is NhatKi)
                        ((NhatKi)F).isShow = true;
                    if (F is Sach)
                        ((Sach)F).isShow = true;
                    if (F is Note)
                    {
                        ((Note)F).UpDate();
                        ((Note)F).isShow = true;
                    }
                    this.Close();
                }
                else
                {
                    if (i > 1)
                    {
                        textBox1.Enabled = false;
                        textBox1.Text = "";
                        old_Time = DateTime.Now;
                        timer1.Start();
                    }
                    ++i;
                    MSG.Text = "Sai mật khẩu lần thứ " + i.ToString();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!textBox1.Enabled)
            {
                if ((DateTime.Now - old_Time).TotalSeconds > i * 5)
                {
                    textBox1.Enabled = true;
                    timer1.Stop();
                }
                else
                    MSG.Text ="Xin đợi: " + (i*5 -(int)(DateTime.Now - old_Time).TotalSeconds).ToString() + "Giây";
            }
        }

        private void ComfixPass_FormClosed(object sender, FormClosedEventArgs e)
        {
            isShow = false;
        }
    }
}
