﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_1
{
    public partial class QuyenPass : Form
    {
        public QuyenPass()
        {
            InitializeComponent();
        }
        //.....................................................
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (Form1.Acc.FillAcc(textBox1.Text))
            {
                B1 = true;
                MSG_1.Text = "";
            }
            else
            {
                MSG_1.Text = "Tài khoản không tồn tại";
                B1 = false;
            }
        }
        bool B1 = false, B2 = false,B3 = false,B4 = false;
        private void button1_Click(object sender, EventArgs e)
        {
            if (B1 & B2 & B3 & B4 & textBox3.Text == textBox2.Text)
            {
                if (Form1.Acc.KT_Pass2(textBox4.Text, textBox1.Text))
                {
                    Form1.Acc.Doi_Pass(textBox3.Text, textBox1.Text);
                    this.Close();
                }
                else
                    MSG_1.Text = "Sai mật khẩu cấp 2";
            }
            else
                MSG_1.Text = "hoàn thiện thông tin yêu cầu";
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (textBox3.Text.Length < 6 & textBox3.Text == textBox1.Text & textBox3.Text == textBox4.Text)
            {
                B2 = false;
                MSG_1.Text = "Mật khẩu phải lớn hơn 6 kí tự, khác tài khoản và khác mật khẩu cấp 2";
            }
            else
            {
                MSG_1.Text = "";
                B2 = true;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (textBox4.Text.Length < 6)
            {
                B3 = false;
                MSG_1.Text = "Mật khẩu cấp 2 phải dài hơn 6 kí tự";
            }
            else
            {
                MSG_1.Text = "";
                B3 = true;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text != textBox3.Text)
            {
                B4 = false;
                MSG_1.Text = "Xác nhận mật khẩu cấp 2 không khớp";
            }
            else
            {
                MSG_1.Text = "";
                B4 = true;
            }
        }


        //..................................................................
        bool C1 = false, C2 = false, C3 = false, C4 = false;
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (Form1.Acc.FillAcc(textBox5.Text))
            {
                C1 = true;
                MSG_2.Text = "";
                textBox9.Text = Form1.Acc.Cau_Hoi(textBox5.Text);
            }
            else
            {
                MSG_2.Text = "Tài khoản không tồn tại";
                C1 = false;
                textBox9.Text = "";
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (textBox6.Text.Length < 6)
            {
                C2 = false;
                MSG_2.Text = "Câu trả lời phải dài hơn 6 kí tự";
            }
            else
            {
                MSG_2.Text = "";
                C2 = true;
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            if (textBox7.Text.Length < 6 & textBox7.Text == textBox5.Text)
            {
                C3 = false;
                MSG_2.Text = "Mật khẩu phải lớn hơn 6 kí tự và khác tài khoản";
            }
            else
            {
                MSG_2.Text = "";
                C3 = true;
            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            if (textBox8.Text != textBox7.Text)
            {
                C4 = false;
                MSG_2.Text = "Xác nhận mật khẩu cấp 2 không khớp";
            }
            else
            {
                MSG_2.Text = "";
                C4 = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (C1 & C2 & C3 & C4 & textBox8.Text == textBox7.Text)
            {
                if (Form1.Acc.KT_Tra_Loi(textBox6.Text, textBox5.Text))
                {
                    Form1.Acc.Doi_Pass(textBox7.Text, textBox5.Text);
                    this.Close();
                }
                else
                    MSG_2.Text = "Câu trả lời không chính xác";
            }
            else
                MSG_2.Text = "Hoàn thiện thông tin yêu cầu";
        }

        private void QuyenPass_Load(object sender, EventArgs e)
        {
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Width - 450, 45);
        }

        

    }
}
