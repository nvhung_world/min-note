﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using DTB;
namespace Project_1
{
    public partial class TaiKhoan : Form
    {
        public bool isShow = false;
        DTB_Connect con;
        DTB_TaiKhoan TK;
        eCuaSo Cs;
        public TaiKhoan()
        {
            InitializeComponent();
            toolTip1.SetToolTip(button1, "Lưu chỉnh sửa");
            toolTip2.SetToolTip(button2, " Quản lý thông tin đăng nhập");
            timer1.Start();
        }
        public TaiKhoan(DTB_Connect con,DTB_TaiKhoan TK,eCuaSo Cs)
        {
            InitializeComponent();
            toolTip1.SetToolTip(button1, "Lưu chỉnh sửa");
            toolTip2.SetToolTip(button2, " Quản lý thông tin đăng nhập");

            this.con = con;
            this.TK = TK;
            this.Cs = Cs;

            textBox1.Text = TK.Data[0];
            textBox2.Text = TK.Data[1];
            if (TK.Data[2] == "Nam")
                checkBox1.Checked = true;
            else
                checkBox2.Checked = true;
            textBox4.Text = TK.Data[3];
            textBox5.Text = TK.Data[4];
            textBox6.Text = TK.Data[5];
            textBox7.Text = TK.Data[6];
            textBox8.Text = TK.Data[7];
            textBox9.Text = TK.Data[8];
            richTextBox1.Text = TK.Data[9];

            button1.Enabled = false;
            timer1.Start();
        }

        private void TaiKhoan_Load(object sender, EventArgs e)
        {
            if(Cs.ViTri_X  > -1 & Cs.ViTri_Y > -1)
                Location = new Point(Cs.ViTri_X, Cs.ViTri_Y);
            if (DTB_Acc.Pass2 == "")
                textBox14.Enabled = false;
            if (DTB_Acc.CauHoi == "")
            {
                textBox15.Enabled = false;
                textBox17.Enabled = false;
            }
            else
                textBox17.Text = DTB_Acc.CauHoi;
        }
        /// <summary>
        /// 
        /// </summary>
        DateTime OldTime;
        private void button2_Click(object sender, EventArgs e)
        {
            isDown = !isDown;
            OldTime = DateTime.Now;
            if (isDown)
            {
                button2.BackgroundImage = global::Project_1.Properties.Resources._798df;
            }
            else
            {
                button2.BackgroundImage = global::Project_1.Properties.Resources._31132;
            }
        }
        bool isDown = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            float D = DateTime.Now.Second * 1000 + DateTime.Now.Millisecond - OldTime.Second * 1000 - OldTime.Millisecond;
            D = D / 1000;
            int i = (int)(600 * D);
            if (isDown)
            {
                if (Size.Width < 695)
                    this.Size = new Size(Size.Width + i, Size.Height);
                if (Size.Width > 695)
                    this.Size = new Size( 695, Size.Height);
            }
            else
            {
                if (Size.Width > 370)
                    this.Size = new Size(Size.Width - i, Size.Height);
                if (Size.Width < 370)
                    this.Size = new Size(370, Size.Height);
            }
            OldTime = DateTime.Now;
        }
        /// <summary>
        /// 
        /// </summary>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MSG.Text = "";
            TK.Data[0] = textBox1.Text;
            TK.Data[1] = textBox2.Text;
            TK.Data[4] = textBox5.Text;
            TK.Data[5] = textBox6.Text;
            TK.Data[6] = textBox7.Text;
            TK.Data[7] = textBox8.Text;
            TK.Data[8] = textBox9.Text;
            TK.Data[9] = richTextBox1.Text;
            if (checkBox1.Checked)
                TK.Data[2] = "Nam";
            else
                TK.Data[2] = "Nữ";            
            if (textBox4.Text != null)
            {
                try
                {
                    DateTime d = Convert.ToDateTime(textBox4.Text);
                    TK.Data[3] = d.Day.ToString() + "/" + d.Month.ToString() + "/" +d.Year.ToString();
                }
                catch
                {
                    MSG.Text = "Ngày sinh không hợp lệ";
                }
            }
            if (MSG.Text == "")
            {
                con.Command("UPDATE ThongTin Set HoTen = '" + MaHoa.Ma_Hoa(TK.Data[0]) + "'," +
                                        " TenKhac = '" + MaHoa.Ma_Hoa(TK.Data[1]) + "'," +
                                        " GioiTinh = '" + MaHoa.Ma_Hoa(TK.Data[2]) + "'," +
                                        " NgaySinh = '" + MaHoa.Ma_Hoa(TK.Data[3]) + "'," +
                                        " DiaChi = '" + MaHoa.Ma_Hoa(TK.Data[4]) + "'," +
                                        " SoDT = '" + MaHoa.Ma_Hoa(TK.Data[5]) + "'," +
                                        " CMT = '" + MaHoa.Ma_Hoa(TK.Data[6]) + "'," +
                                        " Mail = '" + MaHoa.Ma_Hoa(TK.Data[7]) + "'," +
                                        " NgheNghiep = '" + MaHoa.Ma_Hoa(TK.Data[8]) + "', " +
                                        " MieuTa = '" + MaHoa.Ma_Hoa(TK.Data[9]) + "'" +
                                        " WHERE Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) + "'");
                MSG.Text = "Cập nhật thành công";
                button1.Enabled = false;
            }
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                checkBox2.Checked = false;
            else
                checkBox2.Checked = true;
        }

        private void checkBox2_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
                checkBox1.Checked = false;
            else
                checkBox1.Checked = true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            MSG_2.Text = "";
            if (textBox3.Text != DTB_Acc.Pass)
            {
                MSG_2.Text = "Sai mật khẩu !";
                return;
            }
            if (textBox10.Text.Length < 6 & textBox10.Text == DTB_Acc.Acc)
            {
                MSG_2.Text = "Mật khẩu mới không hợp lệ !";
                return;
            }
            if (textBox11.Text != textBox11.Text)
            {
                MSG_2.Text = "Xác nhận không khớp !";
                return;
            }
            con.Command("UPDATE Acc Set Pass = '" + MaHoa.Ma_Hoa(textBox10.Text) +
                                "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) + "'");
            DTB_Acc.Pass = textBox10.Text;
            MSG_2.Text = "Cập nhật thành công ";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MSG_3.Text = "";
            if (DTB_Acc.Pass2 != "" && textBox14.Text != DTB_Acc.Pass2)
            {
                MSG_3.Text = "Mật khẩu cấp 2 không đúng ! ";
                return;
            }
            if (textBox13.Text.Length < 6)
            {
                MSG_3.Text = "Mật khẩu cấp 2 phải dài hơn 6 kí tự ! ";
                return;
            }
            if (textBox12.Text != textBox13.Text)
            {
                MSG_3.Text = "Xác nhận không khớp ! ";
                return;
            }
            con.Command("UPDATE Acc Set Pass2 = '" + MaHoa.Ma_Hoa(textBox13.Text) +
                                "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) + "'");
            DTB_Acc.Pass2 = textBox13.Text;
            MSG_3.Text = "Cập nhật thành công ";
            textBox14.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MSG_4.Text = "";
            if (textBox15.Text != DTB_Acc.TraLoi)
            {
                MSG_4.Text = "Câu trả lời sai !";
                return;
            }
            if (comboBox2.Text == "")
            {
                MSG_4.Text = "Chọn 1 câu hỏi !";
                return;
            }
            if (textBox16.Text.Length < 6)
            {
                MSG_4.Text = "Câu trả lời phải dài hơn 6 kí tự !";
                return;
            }
            con.Command("UPDATE Acc Set CauHoi = '" + MaHoa.Ma_Hoa(comboBox2.Text) +
                                "', TraLoi = '" + MaHoa.Ma_Hoa(textBox16.Text) +
                                "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) + "'");
            DTB_Acc.TraLoi = textBox16.Text;
            DTB_Acc.CauHoi = comboBox2.Text;
            MSG_4.Text = "Cập nhật thành công ";
            textBox17.Enabled = true;
            textBox15.Enabled = true;
            textBox17.Text = DTB_Acc.CauHoi;
        }

        private void TaiKhoan_LocationChanged(object sender, EventArgs e)
        {
            con.Command("UPDATE ViTriCuaSo Set ViTri_X = '" + MaHoa.Ma_Hoa(Location.X.ToString()) +
                                                        "', ViTri_Y = '" + MaHoa.Ma_Hoa(Location.Y.ToString()) +
                                                        "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                        "' and TenCuaSo = '" + MaHoa.Ma_Hoa("TaiKhoan") + "'");
            Cs.ViTri_X = Location.X;
            Cs.ViTri_Y = Location.Y;
        }
    }
}
