﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
namespace Project_1
{
    public partial class Note : Form
    {
        public bool isShow = false;
        public Note()
        {
            InitializeComponent();
        }
        public DTB_Connect con;
        public DTB_Note NO;
        eCuaSo Cs;
        public Note(DTB_Connect con, DTB_Note NO, eCuaSo Cs)
        {
            InitializeComponent();
            this.con = con;
            this.NO = NO;
            this.Cs = Cs;
        }
        //.............................................................
        private void Note_Load(object sender, EventArgs e)
        {
            foreach (eNote E in NO.Data)
            {
                comboBox1.Items.Add(E.TieuDe);
            }
            ccT_Button1.Hide();
        }
        private void QuanHe_LocationChanged(object sender, EventArgs e)
        {
            con.Command("UPDATE ViTriCuaSo Set ViTri_X = '" + MaHoa.Ma_Hoa(Location.X.ToString()) +
                                                        "', ViTri_Y = '" + MaHoa.Ma_Hoa(Location.Y.ToString()) +
                                                        "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                        "' and TenCuaSo = '" + MaHoa.Ma_Hoa("Note") + "'");
            Cs.ViTri_X = Location.X;
            Cs.ViTri_Y = Location.Y;
        }
        private void hScrollBar1_MouseCaptureChanged(object sender, EventArgs e)
        {
            Cs.Opacity = hScrollBar1.Value;
            this.Opacity = ((double)Cs.Opacity) / 100;
            con.Command("UPDATE ViTriCuaSo Set Opacity = '" +
                                                    MaHoa.Ma_Hoa(hScrollBar1.Value.ToString()) +
                                                    "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                    "' and TenCuaSo = '" + MaHoa.Ma_Hoa("Note") + "'");
        }
        public void UpDate()
        {
            hScrollBar1.Value = Cs.Opacity;
            this.Opacity = ((double)Cs.Opacity) / 100;

            if (Cs.ViTri_X > -1 & Cs.ViTri_Y > -1)
                Location = new Point(Cs.ViTri_X, Cs.ViTri_Y);
        }
        //.................................................................
        int SelectedIndex = -1;
        private void button1_Click(object sender, EventArgs e)
        {
            if (SelectedIndex < 0)
            {
                if (comboBox1.Text == "")
                {
                    MessageBox.Show("Tiêu đề không được bỏ trống!");
                    return;
                }
                if (richTextBox1.Text == "")
                {
                    MessageBox.Show("Nội dung không được bỏ trống!");
                    return;
                }

                eNote E = new eNote(con);
                E.TieuDe = comboBox1.Text;
                E.Data = richTextBox1.Text;
                E.ID = NO.MaxID + 1;
                NO.MaxID += 1;

                E.InSert();
                NO.Data.Insert(0,E);
                comboBox1.Items.Insert(0, E.TieuDe);

                comboBox1.Text = "";
                richTextBox1.Text = "";
                comboBox1.SelectedIndex = -1;
            }
            else
            {
                if (comboBox1.Text == "")
                {
                    MessageBox.Show("Tiêu đề không được bỏ trống!");
                    return;
                }
                if (richTextBox1.Text == "")
                {
                    MessageBox.Show("Nội dung không được bỏ trống!");
                    return;
                }
                eNote E = (eNote) NO.Data[SelectedIndex];
                E.TieuDe = comboBox1.Text;
                E.Data = richTextBox1.Text;
                E.UpDate();
                comboBox1.Items[SelectedIndex] = comboBox1.Text;

                comboBox1.Text = "";
                richTextBox1.Text = "";
                comboBox1.SelectedIndex = -1;
                SelectedIndex = -1;
                ccT_Button1.Hide();
            }
        }
        //................................................................
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex >= 0)
            {
                richTextBox1.Text = ((eNote)NO.Data[comboBox1.SelectedIndex]).Data;
                ccT_Button1.Show();
                SelectedIndex = comboBox1.SelectedIndex;
            }
        }

        private void ccT_Button1_OnButtonXoa(bool B)
        {
            if(B)
            {
                ((eNote)NO.Data[SelectedIndex]).Delete();
                NO.Data.RemoveAt(SelectedIndex);
                comboBox1.Items.RemoveAt(SelectedIndex);
                comboBox1.Text = "";
                richTextBox1.Text = "";
                SelectedIndex = -1;
                ccT_Button1.Hide();
            }
        }
        //.............................................................
    }
}
