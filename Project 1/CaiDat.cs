﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
using Microsoft.Win32;
namespace Project_1
{
    public partial class CaiDat : Form
    {
        public bool isShow = false;
        public DTB_Connect con;
        DTB_CaiDat CD;
        eCuaSo Cs;
        public CaiDat()
        {
            InitializeComponent();
        }
        public CaiDat(DTB_Connect con,DTB_CaiDat CD,eCuaSo Cs)
        {
            InitializeComponent();
            this.con = con;
            this.CD = CD;
            this.Cs = Cs;

        }
        private void CaiDat_Load(object sender, EventArgs e)
        {
            this.Location = new Point(Cs.ViTri_X, Cs.ViTri_Y);
            checkBox1.Checked = CD.CaiDat.KD;
            checkBox2.Checked = ((eCuaSo)CD.CuaSo[3]).Khoa;
            checkBox3.Checked = ((eCuaSo)CD.CuaSo[4]).Khoa;
            checkBox4.Checked = ((eCuaSo)CD.CuaSo[5]).Khoa;
            checkBox5.Checked = ((eCuaSo)CD.CuaSo[6]).Khoa;

            checkBox9.Checked = ((eCuaSo)CD.CuaSo[3]).Show;

            thanhCuon1.var = CD.CaiDat.Volume;
            thanhCuon5.var = ((eCuaSo)CD.CuaSo[3]).Opacity;
        }

        private void CaiDat_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            con.Command("UPDATE CaiDat Set KhoiDong = '" +
                                                    MaHoa.Ma_Hoa(checkBox1.Checked.ToString()) +
                                                    "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) + "'");
            CD.CaiDat.KD = checkBox1.Checked;
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (checkBox1.Checked)
                rkApp.SetValue("Min Note", Application.ExecutablePath.ToString());
            else
                rkApp.DeleteValue("Min Note", false);
        }
        //..............................................................
        String sCommand;
        eCuaSo eCs;
        CheckBox c;
        bool isDown = false;
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            sCommand = "Note";
            eCs = ((eCuaSo)CD.CuaSo[3]);
            c = checkBox2;
            if (checkBox2.Checked)
                SetChexBox(true);
            else
            {
                checkBox2.Checked = true;
                Down(true);
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            sCommand = "CongViec";
            eCs = ((eCuaSo)CD.CuaSo[4]);
            c = checkBox3;
            if (checkBox3.Checked)
                SetChexBox(true);
            else
            {
                checkBox3.Checked = true;
                Down(true);
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            sCommand = "Sach";
            eCs = ((eCuaSo)CD.CuaSo[5]);
            c = checkBox4;
            if (checkBox4.Checked)
                SetChexBox(true);
            else
            {
                checkBox4.Checked = true;
                Down(true);
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            sCommand = "NhatKi";
            eCs = ((eCuaSo)CD.CuaSo[6]);
            c = checkBox5;
            if (checkBox5.Checked)
                SetChexBox(true);
            else
            {
                checkBox5.Checked = true;
                Down(true);
            }
        }

        public void Down(bool B)
        {
            if (!isDown & B )
            {
                this.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height + 55);
                panel1.Size += new System.Drawing.Size(0, 55);
                panel5.Location = new Point(panel5.Location.X, panel5.Location.Y + 55);
                isDown = B;
            }
            if (isDown & !B)
            {
                this.Size = new System.Drawing.Size(this.Size.Width, this.Size.Height - 55);
                panel1.Size -= new System.Drawing.Size(0, 55);
                panel5.Location = new Point(panel5.Location.X, panel5.Location.Y - 55);
                isDown = B;
            }
        }


        private void textBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox1.Text == DTB.DTB_Acc.Pass)
                {
                    Down(false);
                    SetChexBox(false);
                    i = 0;
                    textBox1.Text = "";
                }
                else
                {
                    if (i > 1)
                    {
                        textBox1.Enabled = false;
                        textBox1.Text = "";
                        old_Time = DateTime.Now;
                        timer1.Start();
                    }
                    ++i;
                    MSG.Text = "Sai mật khẩu lần thứ " + i.ToString();
                }
            }
        }
        DateTime old_Time;
        int i = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!textBox1.Enabled)
            {
                if ((DateTime.Now - old_Time).TotalSeconds > i * 5)
                {
                    textBox1.Enabled = true;
                    timer1.Stop();
                }
                else
                    MSG.Text = "Xin đợi: " + (i * 5 - (int)(DateTime.Now - old_Time).TotalSeconds).ToString() + "Giây";
            }
        }


        public void SetChexBox(bool B)
        {
            con.Command("UPDATE ViTriCuaSo Set Khoa = '" +
                                                    MaHoa.Ma_Hoa(B.ToString()) +
                                                    "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                    "' and TenCuaSo = '" + MaHoa.Ma_Hoa(sCommand) + "'");
            eCs.Khoa = B;
            c.Checked = B;
        }
        //........................................................

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            con.Command("UPDATE ViTriCuaSo Set Show = '" +
                                                    MaHoa.Ma_Hoa(checkBox9.Checked.ToString()) +
                                                    "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                    "' and TenCuaSo = '" + MaHoa.Ma_Hoa("Note") + "'");
            ((eCuaSo)CD.CuaSo[3]).Show = checkBox9.Checked;
        }

        private void CaiDat_LocationChanged(object sender, EventArgs e)
        {
                con.Command("UPDATE ViTriCuaSo Set ViTri_X = '" + MaHoa.Ma_Hoa(Location.X.ToString()) +
                                                        "', ViTri_Y = '" + MaHoa.Ma_Hoa(Location.Y.ToString()) +
                                                        "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                        "' and TenCuaSo = '" + MaHoa.Ma_Hoa("CaiDat") + "'");
                Cs.ViTri_X = Location.X;
                Cs.ViTri_Y = Location.Y;
        }

        private void thanhCuon1_OnVarChange(object Object)
        {
            con.Command("UPDATE CaiDat Set AmLuong = '" +
                                                    MaHoa.Ma_Hoa(thanhCuon1.var.ToString()) +
                                                    "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) + "'");
            CD.CaiDat.Volume = thanhCuon1.var;
        }

        private void thanhCuon5_OnVarChange(object Object)
        {
            con.Command("UPDATE ViTriCuaSo Set Opacity = '" +
                                                    MaHoa.Ma_Hoa(thanhCuon5.var.ToString()) +
                                                    "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                    "' and TenCuaSo = '" + MaHoa.Ma_Hoa("Note") + "'");
            ((eCuaSo)CD.CuaSo[3]).Opacity = thanhCuon5.var;
        }


    }
}
