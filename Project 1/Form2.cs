﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
using System.Data.SQLite;
using System.Collections;
namespace Project_1
{
    public partial class Form2 : Form
    {

        public DTB_Connect con;
        public DTB_CaiDat CaiDat;
        public DTB_TaiKhoan TaiKhoan;
        public DTB_CongViec CongViec;
        public DTB_NhatKi NhatKi;
        public DTB_Note Note;
        public DTB_Sach Sach;

        Note F_Note;
        CongViec F_CongViec;
        Sach F_Sach;
        NhatKi F_NhatKi;
        CaiDat F_CaiDat;
        TaiKhoan F_TaiKhoan;

        public Form2()
        {
            InitializeComponent();
            //..................................................
            timer1.Start();
            //...................................................
            toolTip1.SetToolTip(button1, "Đóng Min note");
            toolTip2.SetToolTip(button3, "Cài đặt");
            toolTip3.SetToolTip(button2, "Tài khoản");
            toolTip4.SetToolTip(button4, "Mở rộng");
            toolTip5.SetToolTip(button5, "Note");
            toolTip6.SetToolTip(button6, "Nhắc nhở - Công việc");
            toolTip7.SetToolTip(button7, "Câu chuyện của bạn");
            toolTip8.SetToolTip(button8, "Nhật ký");

            //......................
            con = new DTB_Connect();
            CaiDat = new DTB_CaiDat(con.con);
            TaiKhoan = new DTB_TaiKhoan(con.con);

        }
        //..............................................................
        LapDuLieu L;
        public void Loat()
        {
            L = new LapDuLieu(con,this);
            L.Show();
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            int x = (((eCuaSo)CaiDat.CuaSo[0]).ViTri_X > 0 ? ((eCuaSo)CaiDat.CuaSo[0]).ViTri_X : Screen.PrimaryScreen.Bounds.Width - 280);
            int y = (((eCuaSo)CaiDat.CuaSo[0]).ViTri_Y > 0 ? ((eCuaSo)CaiDat.CuaSo[0]).ViTri_Y : 30);
            Location = new Point(x, y);

            Enabled = false;
            Loat();
        }
        public void Loat_Finalli()
        {
            Enabled = true;
            CongViec = L.CongViec;
            NhatKi = L.NhatKi;
            Note = L.Note;
            Sach = L.Sach;

            timer2.Start();

            F_TaiKhoan = new TaiKhoan(con, TaiKhoan, (eCuaSo)CaiDat.CuaSo[1]);
            F_CaiDat = new CaiDat(con, CaiDat, (eCuaSo)CaiDat.CuaSo[2]);
            F_Note = new Note(con, Note, (eCuaSo)CaiDat.CuaSo[3]);
            F_CongViec = new CongViec(con, CongViec, (eCuaSo)CaiDat.CuaSo[4]);
            F_Sach = new Sach(con, Sach, (eCuaSo)CaiDat.CuaSo[5]);
            F_NhatKi = new NhatKi(con, NhatKi, (eCuaSo)CaiDat.CuaSo[6]);

            if (((eCuaSo)CaiDat.CuaSo[3]).Show)
                ShowNote();
        }

        //.........................................................
        bool isDown = false;
        private void button4_Click(object sender, EventArgs e)
        {
            isDown = !isDown;
            OldTime = DateTime.Now;
            if (isDown)
            {
                button4.BackgroundImage = global::Project_1.Properties.Resources.se546;
            }
            else
            {
                button4.BackgroundImage = global::Project_1.Properties.Resources.Download;
            }
        }
        DateTime OldTime;
        private void timer1_Tick(object sender, EventArgs e)
        {
            float D = DateTime.Now.Second*1000 + DateTime.Now.Millisecond - OldTime.Second*1000 - OldTime.Millisecond;
            D = D/1000;
            int i = (int)(500 * D);
            if (isDown)
            {
                if( Size.Height < 240)
                    this.Size = new Size(Size.Width, Size.Height + i);
                if (Size.Height > 240)
                    this.Size = new Size(Size.Width, 240);
            }
            else
            {
                if (Size.Height > 50)
                    this.Size = new Size(Size.Width, Size.Height - i);
                if (Size.Height < 50)
                    this.Size = new Size(Size.Width, 50);
            }
            OldTime = DateTime.Now;
        }


        //...............................................................
        bool isMovie = false;
        private void Form2_MouseDown(object sender, MouseEventArgs e)
        {
            isMovie = true;
            Old_X = MousePosition.X;
            Old_Y = MousePosition.Y;
        }
        private void Form2_MouseUp(object sender, MouseEventArgs e)
        {
            isMovie = false;
            con.Command("UPDATE ViTriCuaSo Set ViTri_X = '" + MaHoa.Ma_Hoa(Location.X.ToString()) +
                                                        "', ViTri_Y = '" + MaHoa.Ma_Hoa(Location.Y.ToString()) +
                                                        "' where Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) +
                                                        "' and TenCuaSo = '" + MaHoa.Ma_Hoa("Form2") + "'");
            ((eCuaSo)CaiDat.CuaSo[0]).ViTri_X = Location.X;
            ((eCuaSo)CaiDat.CuaSo[0]).ViTri_Y = Location.Y;
        }
        int Old_X, Old_Y;
        private void Form2_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMovie)
            {
                Location = new Point(Location.X + (MousePosition.X - Old_X),
                                        Location.Y + (MousePosition.Y - Old_Y));
                if (C != null)
                    C.Location = new Point(Location.X, Location.Y + 240);
                if(L != null)
                    L.Location = new Point(Location.X, Location.Y + 50);
                Old_X = MousePosition.X;
                Old_Y = MousePosition.Y;
            }
        }

        //.....................................................................

        private void button13_Click(object sender, EventArgs e)
        {
            DangXuat();
        }
        public void DangXuat()
        {
            con.Command("UPDATE CaiDatDangNhap Set GiaTri = '" + MaHoa.Ma_Hoa("False") + "' where Name = 'GhiNho'");
            DTB_CaiDatDangNhap.C.table.Rows[0]["GiaTri"] = MaHoa.Ma_Hoa("False");
            DTB_Acc.Dang_Xuat();
            Program.luong = 1;
            Program.B2 = !Program.B2;
            Close();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            con.Close();
            Program.B1 = !Program.B1;

            F_CaiDat.Close();
            F_CongViec.Close();
            F_NhatKi.Close();
            F_Note.Close();
            F_Sach.Close();
            F_TaiKhoan.Close();

            foreach (Form TB in this.TB)
            {
                if (!TB.IsDisposed)
                    TB.Close();
            }
        }

        //.....................................................................

        ComfixPass C;
        private void button2_Click(object sender, EventArgs e)
        {
            if(F_TaiKhoan.IsDisposed)
                F_TaiKhoan = new TaiKhoan(con, TaiKhoan, (eCuaSo)CaiDat.CuaSo[1]);
            if (F_TaiKhoan.isShow)
            {
                F_TaiKhoan.Hide();
                F_TaiKhoan.isShow = false;
            }
            else
            {
                F_TaiKhoan.isShow = true;
                F_TaiKhoan.Show();
            }
            
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if(F_CaiDat.IsDisposed)
                F_CaiDat = new CaiDat(con, CaiDat, (eCuaSo)CaiDat.CuaSo[2]);
            if (F_CaiDat.isShow)
            {
                F_CaiDat.isShow = false;
                F_CaiDat.Hide();
            }
            else
            {
                F_CaiDat.isShow = true;
                F_CaiDat.Show();
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            ShowNote();
        }
        private void ShowNote()
        {
            if (F_Note.IsDisposed)
                F_Note = new Note(con, Note, (eCuaSo)CaiDat.CuaSo[3]);
            if (F_Note.isShow)
            {
                F_Note.Hide();
                F_Note.isShow = false;
            }
            else
            {
                if (((eCuaSo)CaiDat.CuaSo[3]).Khoa)
                {
                    if (C == null || C.IsDisposed || C.F != (Form)F_Note)
                    {
                        if (C != null && !C.IsDisposed && C.F != (Form)F_Note)
                        {
                            C.Close();
                        }
                        C = new ComfixPass(F_Note, new Point(Location.X, Location.Y + 240));
                    }
                    if (!C.isShow)
                    {
                        C.Show();
                        C.isShow = true;
                    }
                }
                else
                {
                    F_Note.Show();
                    F_Note.UpDate();
                    F_Note.isShow = true;
                }
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (F_CongViec.IsDisposed)
                F_CongViec = new CongViec(con, CongViec, (eCuaSo)CaiDat.CuaSo[4]);
            if (F_CongViec.isShow)
            {
                F_CongViec.Hide();
                F_CongViec.isShow = false;
            }
            else
            {
                if (((eCuaSo)CaiDat.CuaSo[4]).Khoa)
                {
                    if (C == null || C.IsDisposed || C.F != (Form)F_CongViec)
                    {
                        if (C != null && !C.IsDisposed &&C.F != (Form)F_CongViec)
                        {
                            C.Close();
                        }
                        C = new ComfixPass(F_CongViec, new Point(Location.X, Location.Y + 240));
                    }
                    if (!C.isShow)
                    {
                        C.Show();
                        C.isShow = true;
                    }
                }
                else
                {
                    F_CongViec.Show();
                    F_CongViec.isShow = true;
                }
            }
        }
        private void button7_Click(object sender, EventArgs e)
        {
            if (F_Sach.IsDisposed)
                F_Sach = new Sach(con, Sach, (eCuaSo)CaiDat.CuaSo[5]);
            if (F_Sach.isShow)
            {
                F_Sach.Hide();
                F_Sach.isShow = false;
            }
            else
            {
                if (((eCuaSo)CaiDat.CuaSo[5]).Khoa)
                {
                    if (C == null || C.IsDisposed || C.F != (Form)F_Sach)
                    {
                        if (C != null && !C.IsDisposed && C.F != (Form)F_Sach)
                        {
                            C.Close();
                        }
                        C = new ComfixPass(F_Sach, new Point(Location.X, Location.Y + 240));
                    }
                    if (!C.isShow)
                    {
                        C.Show();
                        C.isShow = true;
                    }
                }
                else
                {
                    F_Sach.Show();
                    F_Sach.isShow = true;
                }
            }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            if (F_NhatKi.IsDisposed)
                F_NhatKi =new NhatKi(con, NhatKi , (eCuaSo)CaiDat.CuaSo[6]);
            if (F_NhatKi.isShow)
            {
                F_NhatKi.Hide();
                F_NhatKi.isShow = false;
            }
            else
            {
                if (((eCuaSo)CaiDat.CuaSo[6]).Khoa)
                {
                    if (C == null || C.IsDisposed || C.F != (Form)F_NhatKi)
                    {
                        if (C != null && !C.IsDisposed && C.F != (Form)F_NhatKi)
                        {
                            C.Close();
                        }
                        C = new ComfixPass(F_NhatKi, new Point(Location.X, Location.Y + 240));
                    }
                    if (!C.isShow)
                    {
                        C.Show();
                        C.isShow = true;
                    }
                }
                else
                {
                    F_NhatKi.Show();
                    F_NhatKi.isShow = true;
                }
            }
        }
        //....................................................................

        ArrayList TB = new ArrayList();
        private void timer2_Tick(object sender, EventArgs e)
        {
            int  j = 0;
            while (j < TB.Count)
                if (((Form)TB[j]).IsDisposed)
                    TB.RemoveAt(j);
                else
                    j += 1;
            CongViec.Update();
            ArrayList A = CongViec.GetCongViec();
            if(A.Count > 0)
                for (int i = 0; i < A.Count; i++)
                {
                    TB.Add(new ThongBao((eCongViec)A[i], TB.Count, ((eCuaSo)CaiDat.CuaSo[4]).Khoa,this,CaiDat.CaiDat.Volume));
                    ((Form)TB[TB.Count - 1]).Show();
                    
                }
        }


    }
}
