﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
namespace CCT
{
    public partial class CCT_Loat : UserControl
    {
        public CCT_Loat()
        {
            InitializeComponent();
            timer1.Start();
            X = 0;
            Lgb = new LinearGradientBrush(new Rectangle(0, 0, Width, Height),
                                                                Color.Orange,
                                                                Color.Red,
                                                                LinearGradientMode.ForwardDiagonal);
        }
        int X;
        LinearGradientBrush Lgb;
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.FillEllipse(Lgb, X, 0, Height, Height);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (X > Size.Width - 20)
                X = 0;
            X += 2;
            this.Invalidate();
        }
    }
}
