﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCT
{
    public partial class CCT_Button : UserControl
    {
        public CCT_Button()
        {
            InitializeComponent();
            button1.Hide();
            button2.Hide();
        }

        public delegate void ButtonLuu();
        public event ButtonLuu OnButtonLuu;
        public delegate void ButtonXoa(bool B);
        public event ButtonXoa OnButtonXoa;

        bool B = false;
        public void UpButton()
        {
            B = !B;
            if (B)
            {
                button3.BackgroundImage = global::CCT.Properties.Resources.tich;
                button3.BackColor = Color.Green;
            }
            else
            {
                button3.BackgroundImage = global::CCT.Properties.Resources.IconsLandVista;
                button3.BackColor = Color.Orange;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (B)
            {
                UpButton();
                if (OnButtonLuu != null)
                    OnButtonLuu();
            }
            else
            {
                button3.Enabled = false;
                button1.Show();
                button2.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button3.Enabled = true;
            button1.Hide();
            button2.Hide();
            if (OnButtonXoa != null)
                OnButtonXoa(true);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button3.Enabled = true;
            button1.Hide();
            button2.Hide();
            if (OnButtonXoa != null)
                OnButtonXoa(false);
        }

        private void CCT_Button_Load(object sender, EventArgs e)
        {
            button3.Size = Size;
            button1.Size = new System.Drawing.Size(Size.Width / 2, Size.Height / 2);
            button1.Location = new Point(0, Size.Height - button1.Size.Height);
            button2.Size = new System.Drawing.Size(Size.Width / 2, Size.Height / 2);
            button2.Location = new Point(Size.Width - button2.Size.Width, Size.Height - button2.Size.Height);
        }
    }
}
