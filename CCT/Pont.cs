﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
namespace CCT
{
    public partial class Pont : UserControl
    {
        public Pont()
        {
            InitializeComponent();
            C_1 = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            C_2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            
        }
        LinearGradientBrush Lgb;
        public Color X, Y;
        public Color C_1
        {
            set
            {
                X = value;
                Invalidate();
            }
            get
            {
                return X;
            }
        }
        public Color C_2
        {
            set
            {
                Y = value;
                Invalidate();
            }
            get
            {
                return Y;
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Lgb = new LinearGradientBrush(new Rectangle(0, 0, Width, Height),
                                                                X,
                                                                Y,
                                                                LinearGradientMode.ForwardDiagonal);
            e.Graphics.FillEllipse(Lgb, new Rectangle(0, 0, Width, Height));
            Lgb.Dispose();
        }
    }
}
