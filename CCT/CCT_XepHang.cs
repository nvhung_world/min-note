﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CCT
{
    public partial class CCT_XepHang : UserControl
    {
        public CCT_XepHang()
        {
            InitializeComponent();

        }
        public CCT_XepHang(int var)
        {
            InitializeComponent();
            this.Var = var;
        }
        //..........................
        public delegate void VarChange(object Obj);
        public event VarChange OnVarChange;

        public int Paramen = 1;
        public int Var
        {
            set
            {
                Paramen = value;
                if (Paramen > para)
                    Paramen = para;
                if (Paramen < 1)
                    Paramen = 1;
                this.Invalidate();
            }
            get
            {
                return Paramen;
            }
        }
        public int para = 5;
        public int leng
        {
            set
            {
                para = value;
                this.Invalidate();
            }
            get
            {
                return para;
            }
        }
        //.................................
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            int i = Width / para;
            for (int j = 0; j < para; j++)
            {
                if( j < Var )
                    e.Graphics.DrawImage(global::CCT.Properties.Resources.Sao2, i * j, 0, Width / para, Width / para);
                else
                    e.Graphics.DrawImage(global::CCT.Properties.Resources.Sao1, i * j, 0, Width / para, Width / para);
            }
        }


        private void CCT_XepHang_MouseDown(object sender, MouseEventArgs e)
        {
            Var = e.X /( Width / para) + 1;
            if (OnVarChange != null)
                OnVarChange(this);
        }

    }
}
