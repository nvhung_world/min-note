﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
namespace CCT
{
    public partial class CCT_Tep : UserControl
    {
        public CCT_Tep()
        {
            InitializeComponent();
        }
        public eTuSach etu;
        public int index;
        public CCT_Tep(eTuSach e,int index)
        {
            InitializeComponent();
            this.etu = e;
            this.index = index;
            label2.Text = "Ngày tạo: " + etu.NgayTao.ToString();
            textBox1.Text = etu.TenSach;
        }
        //.........................................
        
        //.........................................
        public delegate void Open(CCT_Tep CT, eTuSach eTu, int index);
        public event Open OnOpen;
        private void button1_Click(object sender, EventArgs e)
        {
            if (OnOpen != null)
                OnOpen(this, etu,index);
        }
        //.............................................
        public delegate void Xoa(CCT_Tep CT, eTuSach eTu, int index);
        public event Xoa OnXoa;
        private void ccT_Button1_OnButtonXoa(bool B)
        {
            if (OnXoa != null & B)
                OnXoa(this, etu, index);
        }
        //........................................
        private void textBox1_DoubleClick(object sender, EventArgs e)
        {
            Console.WriteLine("gh");
            textBox1.ReadOnly = false;
            ccT_Button1.UpButton();
        }
        private void ccT_Button1_OnButtonLuu()
        {
            if (textBox1.Text != "")
            {
                etu.TenSach = textBox1.Text;
                textBox1.ReadOnly = true;
                etu.UpDate();
            }
        }
        //.......................................................
        private void CCT_Tep_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Name = "ccT_Item1";
            this.Size = new System.Drawing.Size(350, 50);
            this.TabStop = false;
        }
    }
}
