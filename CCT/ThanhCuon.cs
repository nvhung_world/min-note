﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
namespace CCT
{
    public partial class ThanhCuon : UserControl
    {
        public ThanhCuon()
        {
            InitializeComponent();
        }
        //...................
        int Paramen;
        public int var
        {
            set
            {
                Paramen = value;
                if (value > Ma)
                    Paramen = Ma;
                if (value < Mi)
                    Paramen = Mi;
                this.Invalidate();
            }
            get
            {
                return Paramen;
            }
        }
        int Ma, Mi;
        public int MaxVar
        {
            set
            {
                Ma = value;
            }
            get
            {
                return Ma;
            }
        }
        public int MinVar
        {
            set
            {
                Mi = value;
            }
            get
            {
                return Mi;
            }
        }
        //....................
        Color C1, C2_1,C2_2, C3_1,C3_2,C4;
        public Color Len
        {
            set
            {
                C1 = value;
                BackColor = value;
            }
            get
            {
                return C1;
            }
        }
        public Color ThanhNgang_1
        {
            set
            {
                C2_1 = value;
            }
            get
            {
                return C2_1;
            }
        }
        public Color ThanhNgang_2
        {
            set
            {
                C2_2 = value;
            }
            get
            {
                return C2_2;
            }
        }
        public Color Cham_1
        {
            set
            {
                C3_1 = value;
            }
            get
            {
                return C3_1;
            }
        }
        public Color Cham_2
        {
            set
            {
                C3_2 = value;
            }
            get
            {
                return C3_2;
            }
        }
        public Color Chu
        {
            set
            {
                C4 = value;
            }
            get
            {
                return C4;
            }
        }
        //....................
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            LinearGradientBrush Lgb_1 = new LinearGradientBrush(new Rectangle(0, 0, Width, Height),
                                                                C2_1,
                                                                C2_2,
                                                                LinearGradientMode.Vertical);
            e.Graphics.FillRectangle(Lgb_1,
                                        this.Height / 2,
                                        (Height - 8) / 2,
                                        Width - Height,
                                        8);

            LinearGradientBrush Lgb_2 = new LinearGradientBrush(new Rectangle(0, 0, Width, Height),
                                                               C3_1,
                                                               C3_2,
                                                               LinearGradientMode.ForwardDiagonal);
            float i = ((float)Width - Height) / (Ma - Mi);
            e.Graphics.FillEllipse(Lgb_2,
                                        Paramen * i,
                                        0,
                                        Height,
                                        Height);
            Lgb_1.Dispose();
            Lgb_2.Dispose();

            SolidBrush s = new SolidBrush(C4);
            e.Graphics.DrawString(Paramen.ToString(),this.Font,s,new PointF(5,3));
        }
        public delegate void VarChange(object Object);
        public event VarChange OnVarChange;
        //.......................................................
        bool isDown = false;
        private void ThanhCuon_MouseDown(object sender, MouseEventArgs e)
        {
            int i = e.X;
            if (e.X < this.Height / 2)
            {
                i = this.Height/2;
            }
            if (e.X > Width - Height / 2)
            {
                i = Width - Height / 2;
            }
            isDown = true;
            SetPara(i);
        }

        private void ThanhCuon_MouseUp(object sender, MouseEventArgs e)
        {
            isDown = false;
            if (OnVarChange != null)
                OnVarChange(this);
        }

        private void ThanhCuon_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDown)
            {
                int i = e.X;
                if (e.X < this.Height / 2)
                {
                    i = this.Height/2;
                }
                if (e.X > Width - Height / 2)
                {
                    i = Width - Height / 2;
                }
                SetPara(i);
            }
        }

        public void SetPara(int i)
        {
            float j = i - this.Height / 2;
            float f = j / (Width - Height);
            var =(int)( f * (Ma - Mi));
        }
    }
}
