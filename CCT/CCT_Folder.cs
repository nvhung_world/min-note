﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
using System.Collections;
namespace CCT
{
    public partial class CCT_Folder : UserControl
    {
        public CCT_Folder()
        {
            InitializeComponent();
            eBuutton = new ArrayList();
        }
        public ArrayList Data, eBuutton;
        //...............................................................
        public void Start(ArrayList Data)
        {
            this.Data = Data;
            if (Data.Count > 0)
            {
                for (int i = 0; i < Data.Count; i++)
                {
                    if (Data[i] is eTuSach)
                    {
                        CCT_Tep item = new CCT_Tep((eTuSach)Data[i], i);
                        item.OnXoa += new CCT.CCT_Tep.Xoa(this.ccT_Tep1_OnXoa);
                        item.OnOpen += new CCT.CCT_Tep.Open(this.ccT_Tep1_OnOpen);
                        eBuutton.Add(item);
                        this.Controls.Add(item);
                    }
                    else
                    {
                        CCT_Item item = new CCT_Item((eData)Data[i], 0);
                        item.OnXoa += new CCT.CCT_Item.Xoa(this.ccT_Item1_OnXoa);
                        item.OnMoDong += new CCT.CCT_Item.MoDong(this.ccT_Item1_OnMoDong);
                        eBuutton.Add(item);
                        this.Controls.Add(item);
                    }
                }
                Up_Date();
            }
        }
        public void AddButton(eData Item)
        {
            Item.InSert();
            Data.Insert(0, Item);
            CCT_Item item = new CCT_Item(Item,0);
            item.OnXoa += new CCT.CCT_Item.Xoa(this.ccT_Item1_OnXoa);
            item.OnMoDong += new CCT.CCT_Item.MoDong(this.ccT_Item1_OnMoDong);
            eBuutton.Insert(0, item);
            for (int i = 1; i < eBuutton.Count; i++)
            {
                if (eBuutton[i] is CCT_Tep)
                    ((CCT_Tep)eBuutton[i]).index += 1;
                else
                    ((CCT_Item)eBuutton[i]).index += 1;
            }
            this.Controls.Add(item);
            Up_Date();
        }
        public void AddTu(eTuSach Tu)
        {
            Tu.InSert();
            Data.Insert(0,Tu);
            CCT_Tep item = new CCT_Tep(Tu, 0);
            item.OnXoa += new CCT.CCT_Tep.Xoa(this.ccT_Tep1_OnXoa);
            item.OnOpen += new CCT.CCT_Tep.Open(this.ccT_Tep1_OnOpen);
            eBuutton.Insert(0, item);
            for (int i = 1; i < eBuutton.Count; i++)
            {
                if (eBuutton[i] is CCT_Tep)
                    ((CCT_Tep)eBuutton[i]).index += 1;
                else
                    ((CCT_Item)eBuutton[i]).index += 1;
            }
            this.Controls.Add(item);
            Up_Date();
        }
        public void Up_Date()
        {
            int j = eBuutton.Count * 52 + 2;
            if (j >= 380)
                vScrollBar1.Maximum = j - 380;
            else
                vScrollBar1.Maximum = 0;

            for (int i = 0; i < eBuutton.Count; i++)
            {
                ((UserControl)eBuutton[i]).Location = new Point(3, 52 * i + 2 - vScrollBar1.Value);
            }
        }
        public void ClearButton()
        {
            eBuutton.Clear();
            int i =1;
            while(i<Controls.Count)
                this.Controls.RemoveAt(i);
            Data = new ArrayList();
            Up_Date();
        }

        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            if (eBuutton.Count > 0)
            {
                for (int i = 0; i < eBuutton.Count; i++)
                {
                    ((UserControl)eBuutton[i]).Location = new Point(3, 52 * i + 2 - vScrollBar1.Value);
                }
            }
        }
        //.....................................................................
        public delegate void MoDong(CCT_Item UC, eData eItem,int index);
        public event MoDong OnMoDong;
        private void ccT_Item1_OnMoDong(CCT_Item UC, DTB.eData eItem,int index)
        {
            if (OnMoDong != null)
                OnMoDong(UC, eItem,index);
        }

        private void ccT_Item1_OnXoa(CCT_Item UC, DTB.eData eItem,int index)
        {
            eItem.Delete();
            this.Controls.Remove(UC);
            Data.RemoveAt(index);
            ((UserControl)eBuutton[index]).Dispose();
            eBuutton.RemoveAt(index);
            for (int i = index ; i < eBuutton.Count; i++)
                if (eBuutton[i] is CCT_Tep)
                    ((CCT_Tep)eBuutton[i]).index -= 1;
                else
                    ((CCT_Item)eBuutton[i]).index -= 1;

            Up_Date();
        }


        //.......................................................
        public delegate void Open(CCT_Tep CT, eTuSach eItem, int index);
        public event Open OnOpen;
        private void ccT_Tep1_OnOpen(CCT_Tep CT, eTuSach eTu, int index)
        {
            if (OnOpen != null)
                OnOpen(CT, eTu, index);
        }
        private void ccT_Tep1_OnXoa(CCT_Tep CT, eTuSach eTu, int index)
        {
            eTu.Delete();
            this.Controls.Remove(CT);
            Data.RemoveAt(index);
            ((UserControl)eBuutton[index]).Dispose();
            eBuutton.RemoveAt(index);
            for (int i = index ; i < eBuutton.Count; i++)
                if (eBuutton[i] is CCT_Tep)
                {
                    ((CCT_Tep)eBuutton[i]).index -= 1;
                }
                else
                    ((CCT_Item)eBuutton[i]).index -= 1;
            Up_Date();
        }

    }
}
