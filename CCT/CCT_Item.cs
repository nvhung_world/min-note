﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTB;
using System.Collections;
namespace CCT
{
    public partial class CCT_Item : UserControl
    {
        public CCT_Item()
        {
            InitializeComponent();
        }
        public CCT_Item(eData E,int index)
        {
            InitializeComponent();
            this.eItem = E;
            this.index = index;
            try
            {
                label1.Text = (String)(eItem.ToText()[0]);
                label2.Text = (String)(eItem.ToText()[1]);
            }
            catch (Exception Ex)
            {
            }
        }

        private void CCT_Item_Load(object sender, EventArgs e)
        {
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.Name = "ccT_Item1";
            this.Size = new System.Drawing.Size(350, 50);
            this.TabStop = false;
        }

        public eData eItem;
        public int index;
        public void Up_Date()
        {
            eItem.UpDate();
            try
            {
                label1.Text = (String)(eItem.ToText()[0]);
                label2.Text = (String)(eItem.ToText()[1]);
            }
            catch (Exception Ex)
            {
            }
        }
        public delegate void Xoa(CCT_Item UC, eData eItem, int index);
        public event Xoa OnXoa;
        private void ccT_Button1_OnButtonXoa(bool B)
        {
            if (OnXoa != null & B)
                OnXoa(this, eItem, index);
        }

        public delegate void MoDong(CCT_Item UC, eData eItem,int index);
        public event MoDong OnMoDong;
        private void button1_Click(object sender, EventArgs e)
        {
            if (OnMoDong != null)
                OnMoDong(this, eItem,index);
        }

    }
}
