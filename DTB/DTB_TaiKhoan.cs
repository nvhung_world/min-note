﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
namespace DTB
{
    public class DTB_TaiKhoan
    {
        DataTable table;
        public String[] Data;
        public DTB_TaiKhoan(SQLiteConnection con)
        {
            table = new DataTable();
            Data = new String[10];

            SetupTable();
            Loat(con);
            Fill();
        }
        void SetupTable()
        {
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "HoTen";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TenKhac";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "GioTinh";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NgaySinh";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DiaChi";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "SoDT";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CMT";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Mail";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NgheNghiep";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "MieuTa";
            table.Columns.Add(column);
        }

        void Loat(SQLiteConnection con)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM ThongTin", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Fill(table);
                }
            }
        }

        void Fill()
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(DTB_Acc.Acc))
                {
                    Data[0] = MaHoa.Giai_Ma(table.Rows[i]["HoTen"].ToString());
                    Data[1] = MaHoa.Giai_Ma(table.Rows[i]["TenKhac"].ToString());
                    Data[2] = MaHoa.Giai_Ma(table.Rows[i]["GioiTinh"].ToString());
                    Data[3] = MaHoa.Giai_Ma(table.Rows[i]["NgaySinh"].ToString());
                    Data[4] = MaHoa.Giai_Ma(table.Rows[i]["DiaChi"].ToString());
                    Data[5] = MaHoa.Giai_Ma(table.Rows[i]["SoDT"].ToString());
                    Data[6] = MaHoa.Giai_Ma(table.Rows[i]["CMT"].ToString());
                    Data[7] = MaHoa.Giai_Ma(table.Rows[i]["Mail"].ToString());
                    Data[8] = MaHoa.Giai_Ma(table.Rows[i]["NgheNghiep"].ToString());
                    Data[9] = MaHoa.Giai_Ma(table.Rows[i]["MieuTa"].ToString());
                }
                break;
            }
        }
    }
}
