﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Collections;
namespace DTB
{
    
    public class DTB_CaiDat
    {
        DataTable table;
        public eCaiDat CaiDat;
        public ArrayList CuaSo;
        //public eCuaSo[] CuaSo = new eCuaSo[11];
        public DTB_CaiDat(SQLiteConnection con)
        {
            CuaSo = new ArrayList(11);
            CaiDat = new eCaiDat();
            table = new DataTable();

            SetupCaiDat();
            Loat_CD(con);
            Fill_CD();

            SetupCuaSo();
            Loat_CS(con);
            Fill_CS();
        }
        /// <summary>
        /// 
        /// </summary>
        void Fill_CD()
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(DTB_Acc.Acc))
                {
                    CaiDat.KhoiDong = MaHoa.Giai_Ma(table.Rows[i]["KhoiDong"].ToString());
                    try
                    {
                        CaiDat.Volume =Convert.ToInt32( MaHoa.Giai_Ma(table.Rows[i]["AmLuong"].ToString()));
                    }
                    catch (Exception)
                    {
                        CaiDat.Volume = 100;
                    }
                    break;
                }
            }
        }
        void Fill_CS()
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                int j = 0;
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(DTB_Acc.Acc))
                {
                    eCuaSo CuaSo = new eCuaSo();
                    CuaSo.K = MaHoa.Giai_Ma(table.Rows[i]["Khoa"].ToString());
                    CuaSo.TenCuaSo = MaHoa.Giai_Ma(table.Rows[i]["TenCuaSo"].ToString());
                    CuaSo.S = MaHoa.Giai_Ma(table.Rows[i]["Show"].ToString());
                    try
                    {
                        CuaSo.ViTri_Y = Convert.ToInt32( MaHoa.Giai_Ma(table.Rows[i]["ViTri_Y"].ToString()));
                    }
                    catch (Exception)
                    {
                        CuaSo.ViTri_Y = 0;
                    }
                    //..........
                    try
                    {
                        CuaSo.ViTri_X = Convert.ToInt32( MaHoa.Giai_Ma(table.Rows[i]["ViTri_X"].ToString()));
                    }
                    catch (Exception)
                    {
                        CuaSo.ViTri_X = 0;
                    }
                    //.............
                    try
                    {
                        CuaSo.Opacity = Convert.ToInt32(MaHoa.Giai_Ma(table.Rows[i]["Opacity"].ToString()));
                    }
                    catch (Exception)
                    {
                        CuaSo.Opacity = 100;
                    }
                    j += 1;
                    this.CuaSo.Add(CuaSo);
                }
                if (j > 10)
                    break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        void Loat_CD(SQLiteConnection con)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM CaiDat", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Fill(table);
                }
            }
        }
        void Loat_CS(SQLiteConnection con)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM ViTriCuaSo", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Fill(table);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        void SetupCaiDat()
        {
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "KhoiDong";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "AmLuong";
            table.Columns.Add(column);
        }
        void SetupCuaSo()
        {
            table = new DataTable();
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TenCuaSo";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Show";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ViTri_X";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ViTri_Y";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Opacity";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Khoa";
            table.Columns.Add(column);
        }
        /// <summary>
        /// 
        /// </summary>
    }
        public class eCaiDat
        {
            public bool KD;
            public String KhoiDong
            {
                set
                {
                    if (value == "True")
                        KD = true;
                    else
                        KD = false;
                }
                get
                {
                    return KD.ToString();
                }
            }
            public int Volume;
        }
        public class eCuaSo
        {
            public bool Show, Khoa;
            public String TenCuaSo;
            public String S
            {
                set
                {
                    if (value == "True")
                        Show = true;
                    else
                        Show = false;
                }
                get
                {
                    return Show.ToString();
                }
            }
            public String K
            {
                set
                {
                    if(value == "True")
                        Khoa = true;
                    else
                        Khoa = false;
                }
                get
                {
                    return Khoa.ToString();
                }
            }
            public int ViTri_X, ViTri_Y, Opacity;
        }
}
