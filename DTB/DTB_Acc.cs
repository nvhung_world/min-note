﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;
using Microsoft.Win32;
namespace DTB
{
    public class  DTB_Acc
    {
        DataTable table;
        SQLiteConnection con;
        DataRow R;

        SQLiteCommand Cmd;
        //...........................................
        public static int ID = -1;
        public static String Acc,Pass,Pass2,CauHoi,TraLoi;
        //...........................................
        public DTB_Acc()
        {
            
            table = new DataTable("Acc");
            con = new SQLiteConnection("URI=file:" +Application.StartupPath + "/DTB.db");
            Cmd = new SQLiteCommand(con);
            //.................................................

            DataColumn column;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Pass";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Pass2";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "CauHoi";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "TraLoi";
            table.Columns.Add(column);
        }

        public void Open()
        {
            if (con != null)
            {
                con.Open();
                using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM Acc", con))
                {

                    using (new SQLiteCommandBuilder(da))
                    {
                        da.Fill(table);
                    }
                }
            }
        }

        public void Close()
        {
            if (con != null)
            {
                con.Close();
            }
        }
        public void Update()
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM Acc", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Update(table);
                }
            }
        }

        //Acc
        public bool CreateAcc(String[] S)
        {
            R = table.NewRow();
            R["Id"] = table.Rows.Count;
            R["Acc"] =MaHoa.Ma_Hoa(S[0]);
            R["Pass"] = MaHoa.Ma_Hoa(S[1]);
            R["Pass2"] = MaHoa.Ma_Hoa(S[2]);
            R["CauHoi"] = MaHoa.Ma_Hoa(S[3]);
            R["TraLoi"] = MaHoa.Ma_Hoa(S[4]);
            table.Rows.Add(R);
            Update();

            Cmd.CommandText = "INSERT INTO CaiDat (Acc,KhoiDong)VALUES ('" + MaHoa.Ma_Hoa(S[0]) + "', '" +
                                                                            MaHoa.Ma_Hoa("True") + "')";
            Cmd.ExecuteNonQuery();
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rkApp.SetValue("Min Note", Application.ExecutablePath.ToString());

            Cmd.CommandText = "INSERT INTO ThongTin (Acc)VALUES ('" + MaHoa.Ma_Hoa(S[0]) + "')";
            Cmd.ExecuteNonQuery();

            Cmd.CommandText = "INSERT INTO ViTriCuaSo (Acc,TenCuaSo)VALUES " +
                                " ('" + MaHoa.Ma_Hoa(S[0]) + "','" + MaHoa.Ma_Hoa("Form2") + "')";
            Cmd.ExecuteNonQuery();

            Cmd.CommandText = "INSERT INTO ViTriCuaSo (Acc,TenCuaSo)VALUES " +
                                " ('" + MaHoa.Ma_Hoa(S[0]) + "','" + MaHoa.Ma_Hoa("TaiKhoan") + "')";
            Cmd.ExecuteNonQuery();

            Cmd.CommandText = "INSERT INTO ViTriCuaSo (Acc,TenCuaSo)VALUES " +
                                " ('" + MaHoa.Ma_Hoa(S[0]) + "','" + MaHoa.Ma_Hoa("CaiDat") + "')";
            Cmd.ExecuteNonQuery();

            Cmd.CommandText = "INSERT INTO ViTriCuaSo (Acc,TenCuaSo)VALUES " +
                                " ('" + MaHoa.Ma_Hoa(S[0]) + "','" + MaHoa.Ma_Hoa("Note") + "')";
            Cmd.ExecuteNonQuery();

            Cmd.CommandText = "INSERT INTO ViTriCuaSo (Acc,TenCuaSo)VALUES " +
                                " ('" + MaHoa.Ma_Hoa(S[0]) + "','" + MaHoa.Ma_Hoa("CongViec") + "')";
            Cmd.ExecuteNonQuery();

            Cmd.CommandText = "INSERT INTO ViTriCuaSo (Acc,TenCuaSo)VALUES " +
                                " ('" + MaHoa.Ma_Hoa(S[0]) + "','" + MaHoa.Ma_Hoa("Sach") + "')";
            Cmd.ExecuteNonQuery();

            Cmd.CommandText = "INSERT INTO ViTriCuaSo (Acc,TenCuaSo)VALUES " +
                                " ('" + MaHoa.Ma_Hoa(S[0]) + "','" + MaHoa.Ma_Hoa("NhatKi") + "')";
            Cmd.ExecuteNonQuery();
            return true;
        }

        public bool FillAcc(String Acc)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(Acc))
                    return true;
            }
            return false;
        }
        public bool Dang_Nhap(String Acc,String Pass)
        {
            for (int i = 0; i < table.Rows.Count; i++ )
            {
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(Acc))
                    if (table.Rows[i]["Pass"].ToString() == MaHoa.Ma_Hoa(Pass))
                    {
                        DTB_Acc.ID =  (int)table.Rows[i]["Id"];
                        DTB_Acc.Acc = Acc;
                        DTB_Acc.Pass = Pass;
                        DTB_Acc.Pass2 = MaHoa.Giai_Ma( table.Rows[i]["Pass2"].ToString());
                        DTB_Acc.CauHoi = MaHoa.Giai_Ma( table.Rows[i]["CauHoi"].ToString());
                        DTB_Acc.TraLoi = MaHoa.Giai_Ma( table.Rows[i]["TraLoi"].ToString());
                        return true;
                    }
                
            }
            return false;
        }
        public static bool Dang_Xuat()
        {
            ID = -1;
            DTB_Acc.Acc = null;
            DTB_Acc.Pass = null;
            DTB_Acc.Pass2 = null;
            DTB_Acc.CauHoi = null;
            DTB_Acc.TraLoi = null;
            return true;
        }
        //Pass
        public bool KT_Pass(String Pass)
        {
            if (ID >=0)
            if (table.Rows[ID]["Pass"].ToString() == MaHoa.Ma_Hoa(Pass))
                return true;
            return false;
        }
        public bool Doi_Pass(String Pass)
        {
            Cmd.CommandText = "UPDATE Acc Set Pass = '" + MaHoa.Ma_Hoa(Pass) + "' where Id = " + ID.ToString() ;
            Cmd.ExecuteNonQuery();
            table.Rows[ID]["Pass"] = MaHoa.Ma_Hoa(Pass);
            return true;
        }
        public bool Doi_Pass(String Pass,String Acc)
        {
            Cmd.CommandText = "UPDATE Acc Set Pass = '" + MaHoa.Ma_Hoa(Pass) + "' where Acc = '" + MaHoa.Ma_Hoa(Acc) +"'";
            Cmd.ExecuteNonQuery();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(Acc))
                    table.Rows[i]["pass"] = MaHoa.Ma_Hoa(Pass);
            }
            return true;
        }

        //Pass 2
        public bool KT_Pass2(String Pass2, String Acc)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(Acc))
                {
                    if (table.Rows[i]["Pass2"].ToString() == MaHoa.Ma_Hoa(Pass2))
                        return true;
                }
            }
            return false;
        }
        public bool Doi_Pass2(String Pass2)
        {
            Cmd.CommandText = "UPDATE Acc Set Pass2 = '" + MaHoa.Ma_Hoa(Pass2) + "' where Id = " + ID.ToString();
            Cmd.ExecuteNonQuery();
            table.Rows[ID]["Pass2"] = MaHoa.Ma_Hoa(Pass2);
            return true;
        }

        //Cau Hoi
        public String Cau_Hoi()
        {
            return MaHoa.Giai_Ma(table.Rows[ID]["CauHoi"].ToString());
        }
        public String Cau_Hoi(String Acc)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(Acc))
                    return MaHoa.Giai_Ma(table.Rows[i]["CauHoi"].ToString());
            }

            return "";
        }
        public bool Doi_Cau_Hoi(String CauHoi)
        {
            Cmd.CommandText = "UPDATE Acc Set CauHoi = '" + CauHoi + "' where Id = " + ID.ToString();
            Cmd.ExecuteNonQuery();
            table.Rows[ID]["CauHoi"] = CauHoi;
            return true;
        }

        //Tra Loi
        public bool KT_Tra_Loi(String TraLoi)
        {
            if (ID >= 0)
            if (table.Rows[ID]["TraLoi"].ToString() == MaHoa.Ma_Hoa(TraLoi))
                return true;
            return false;
        }
        public bool KT_Tra_Loi(String TraLoi,String Acc)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(Acc))
                    if (MaHoa.Ma_Hoa(TraLoi) == table.Rows[i]["TraLoi"].ToString())
                        return true;
            }
            return false;
        }
        public bool Doi_Tra_Loi(String TraLoi)
        {
            Cmd.CommandText = "UPDATE Acc Set TraLoi = '" + MaHoa.Ma_Hoa(TraLoi) + "' where Id = " + ID.ToString();
            Cmd.ExecuteNonQuery();
            table.Rows[ID]["TraLoi"] = MaHoa.Ma_Hoa(TraLoi);
            return true;
        }
    }
}
