﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Collections;
namespace DTB
{
    public class DTB_Sach
    {
        DataTable table;
        public ArrayList Data;
        public int maxID_TuSach = 0;

        public DTB_Connect con;
        public DTB_Sach(DTB_Connect con)
        {
            Data = new ArrayList();
            table = new DataTable();
            this.con = con;

            SetupTableTuSach();
            LoatTuSach(con.con);
            FillTuSach();

            SetupTableChuong();
            LoatChuong(con.con);
            FillChuong();
        }
        /// <summary>
        /// 
        /// </summary>
        void SetupTableTuSach()
        {
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ID";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TenSach";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NgayTao";
            table.Columns.Add(column);
        }
        void SetupTableChuong()
        {
            table = new DataTable();
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "BaseID";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ID";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TenChuong";
            table.Columns.Add(column);

            
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NgayTao";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "XepHang";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TieuDe";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NoiDung";
            table.Columns.Add(column);
        }
        /// <summary>
        /// 
        /// </summary>
        void LoatTuSach(SQLiteConnection con)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM TuSach", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Fill(table);
                }
            }
        }
        void LoatChuong(SQLiteConnection con)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM Sach", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Fill(table);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        void FillTuSach()
        {
            for (int i = table.Rows.Count - 1; i >=0; i--)
            {
                if(table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(DTB_Acc.Acc))
                {
                    eTuSach e = new eTuSach(con);
                    e.TabData = "Sach";
                    e.ID = Convert.ToInt32(MaHoa.Giai_Ma(table.Rows[i]["ID"].ToString()));
                    e.TenSach = MaHoa.Giai_Ma(table.Rows[i]["TenSach"].ToString());
                    e.Acc = MaHoa.Giai_Ma(table.Rows[i]["Acc"].ToString());
                    e.NgayTao = Convert.ToDateTime( MaHoa.Giai_Ma(table.Rows[i]["NgayTao"].ToString()));
                    e.Data = new ArrayList();
                    if (e.ID > maxID_TuSach)
                        maxID_TuSach = e.ID;
                    Data.Add(e);
                }
            }
        }
        void FillChuong()
        {
            for (int i = table.Rows.Count - 1; i >=0 ; i--)
            {
                if(table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(DTB_Acc.Acc))
                {
                    for (int j = 0; j < Data.Count; j++)
                        if (table.Rows[i]["BaseID"].ToString() == MaHoa.Ma_Hoa(((eTuSach)Data[j]).ID.ToString()))
                        {
                            eChuong e = new eChuong(con);
                            e.BaseID= Convert.ToInt32(MaHoa.Giai_Ma(table.Rows[i]["BaseID"].ToString()));
                            e.ID = Convert.ToInt32(MaHoa.Giai_Ma(table.Rows[i]["ID"].ToString()));
                            e.TenChuong = MaHoa.Giai_Ma(table.Rows[i]["TenChuong"].ToString());
                            e.Acc = MaHoa.Giai_Ma(table.Rows[i]["Acc"].ToString());
                            e.NgayTao = MaHoa.Giai_Ma(table.Rows[i]["NgayTao"].ToString());
                            e.NoiDung = MaHoa.Giai_Ma(table.Rows[i]["NoiDung"].ToString());
                            e.TieuDe = MaHoa.Giai_Ma(table.Rows[i]["TieuDe"].ToString());
                            e.XepHang =Convert.ToInt32( MaHoa.Giai_Ma(table.Rows[i]["XepHang"].ToString()));
                            if (e.ID > ((eTuSach)Data[j]).maxID_Chuong)
                                ((eTuSach)Data[j]).maxID_Chuong = e.ID;
                            ((eTuSach)Data[j]).Data.Add(e);
                        }
                }
            }
        }
    }
    public class eTuSach
    {
        public DTB_Connect con;
        public eTuSach(DTB_Connect con)
        {
            this.con = con;
        }
        int X, Y;
        public int ID
        {
            set
            {
                X = value;
            }
            get
            {
                return X;
            }
        }
        public int BaseID
        {
            set
            {
                Y = value;
            }
            get
            {
                return Y;
            }
        }

        public int maxID_Chuong = 0;

        public String TenSach,Acc,TabData;
        public DateTime NgayTao;
        public ArrayList Data;
        //.......................................
        //.......................................
        public int InSert()
        {
            return con.Command("INSERT INTO TuSach VALUES ('" + MaHoa.Ma_Hoa(ID.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(Acc) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(TenSach) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(NgayTao.ToString()) + "')");
        }
        public int UpDate()
        {
            return con.Command("UPDATE TuSach Set TenSach = '" + MaHoa.Ma_Hoa(TenSach) + "'," +
                                            "NgayTao = '" + MaHoa.Ma_Hoa(NgayTao.ToString()) + "'" +
                                            "Where ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                " Acc = '" + MaHoa.Ma_Hoa(Acc) + "'");
        }
        public int Delete()
        {
            Data.Clear();
            maxID_Chuong = 0;
            con.Command("DELETE FROM TuSach WHERE ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                        "Acc = '" + MaHoa.Ma_Hoa(Acc) + "'");
            return con.Command("DELETE FROM " + TabData + " WHERE BaseID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                                "Acc = '" + MaHoa.Ma_Hoa(Acc) + "'");
        }
        public ArrayList ToText()
        {
            ArrayList A = new ArrayList(3);
            A.Add("Tên sách: " + TenSach);
            A.Add("Ngày Tạo: " + NgayTao.ToString());
            return A;
        }
    }


    public class eChuong : eData
    {
        public DTB_Connect con;
        public eChuong(DTB_Connect con)
        {
            this.con = con;
        }
        public String TenChuong, NgayTao, TieuDe, NoiDung, Acc;
        public int XepHang;
        int X, Y;
        public int ID
        {
            set
            {
                X = value;
            }
            get
            {
                return X;
            }
        }
        public int BaseID
        {
            set
            {
                Y = value;
            }
            get
            {
                return Y;
            }
        }


        public int InSert()
        {
            return con.Command("INSERT INTO Sach VALUES ('" + MaHoa.Ma_Hoa(BaseID.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(ID.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(Acc) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(TenChuong) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(NgayTao) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(XepHang.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(TieuDe) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(NoiDung) + "')");
        }
        public int UpDate()
        {
            return con.Command("UPDATE Sach Set TenChuong = '" + MaHoa.Ma_Hoa(TenChuong.ToString()) + "'," +
                                            "NgayTao = '" + MaHoa.Ma_Hoa(NgayTao) + "'," +
                                            "XepHang = '" + MaHoa.Ma_Hoa(XepHang.ToString()) + "'," +
                                            "TieuDe = '" + MaHoa.Ma_Hoa(TieuDe) + "'," +
                                            "NoiDung = '" + MaHoa.Ma_Hoa(NoiDung) + "'" +
                                            "Where ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                " Acc = '" + MaHoa.Ma_Hoa(Acc) + "' and " +
                                                " BaseID = '" + MaHoa.Ma_Hoa(BaseID.ToString()) + "'");
        }
        public int Delete()
        {
            return con.Command("DELETE FROM Sach Where ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                " Acc = '" + MaHoa.Ma_Hoa(Acc) + "' and " +
                                                " BaseID = '" + MaHoa.Ma_Hoa(BaseID.ToString()) + "'");
        }
        public ArrayList ToText()
        {
            ArrayList A = new ArrayList(3);
            A.Add("Chương: " + TenChuong);
            A.Add("Ngày Tạo: " + NgayTao.ToString());
            return A;
        }
    }
}
