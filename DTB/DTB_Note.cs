﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Collections;
namespace DTB
{
    public class DTB_Note
    {
        public ArrayList Data;
        public int MaxID = 0;
        DataTable table;
        public DTB_Connect con;
        public DTB_Note(DTB_Connect con)
        {
            Data = new ArrayList();
            //......................
            table = new DataTable();
            this.con = con;
            //......................
            SetupTableNote();
            LoatNote(con.con);
            FillNote();
        }
        //...........................
        void SetupTableNote()
        {
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ID";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TieuDe";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Data";
            table.Columns.Add(column);

        }
        void LoatNote(SQLiteConnection con)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM Note", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Fill(table);
                }
            }
        }
        void FillNote()
        {
            for (int i = table.Rows.Count - 1; i >=0; i--)
            {
                if(table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(DTB_Acc.Acc))
                {
                    eNote e = new eNote(con);
                    e.ID = Convert.ToInt32(MaHoa.Giai_Ma(table.Rows[i]["ID"].ToString()));
                    e.Acc = DTB_Acc.Acc;
                    e.TieuDe = MaHoa.Giai_Ma(table.Rows[i]["TieuDe"].ToString());
                    e.Data = MaHoa.Giai_Ma(table.Rows[i]["Data"].ToString());
                    if(MaxID<e.ID)
                        MaxID = e.ID;
                    Data.Add(e);
                }
            }

        }
    }

    public class eNote : eData
    {
        DTB_Connect con;
        public eNote(DTB_Connect con)
        {
            this.con = con;
            TieuDe = "";
            Acc = DTB_Acc.Acc;
            Data = "";
        }
        public String Acc, Data,TieuDe;
        //...................................
        int X, Y;
        public int ID
        {
            set
            {
                X = value;
            }
            get
            {
                return X;
            }
        }
        public int BaseID
        {
            set
            {
                Y = value;
            }
            get
            {
                return Y;
            }
        }

        public int UpDate()
        {
            return con.Command("UPDATE Note Set TieuDe = '" + MaHoa.Ma_Hoa(TieuDe) + "'," +
                                            "Data = '" + MaHoa.Ma_Hoa(Data) + "' " +
                                            "Where ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                " Acc = '" + MaHoa.Ma_Hoa(Acc) + "'");
        }
        public int InSert()
        {
            return con.Command("INSERT INTO Note VALUES ('" + MaHoa.Ma_Hoa(ID.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(Acc) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(TieuDe) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(Data) + "')");
        }
        public int Delete()
        {
            return con.Command("DELETE FROM Note Where ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                     " Acc = '" + MaHoa.Ma_Hoa(Acc) + "'");
        }
        public ArrayList ToText()
        {
            ArrayList A = new ArrayList();
            A.Add(TieuDe);
            return A;
        }

    }
}
