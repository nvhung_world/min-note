﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Collections;

namespace DTB
{
    public class DTB_CongViec
    {
        DataTable table;
        public ArrayList Data;
        DTB_Connect con;
        public int maxID = 0;
        public DTB_CongViec(DTB_Connect con)
        {
            table = new DataTable();
            Data = new ArrayList();
            this.con = con;

            SetupTabel();
            Loat(con.con);
            Fill();
        }
        /// <summary>
        /// 
        /// </summary>
        void SetupTabel()
        {
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ID";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NgayTao";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TieuDe";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "XepHang";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NoiDung";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DateTime";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "LapLai";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "OpenFile";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DongMinNote";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TatMay";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "LastDay";
            table.Columns.Add(column);
        }
        void Loat(SQLiteConnection con)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM CongViec", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Fill(table);
                }
            }
        }
        void Fill()
        {
            for (int i = table.Rows.Count - 1; i >= 0 ; i--)
            {
                if(table.Rows[i]["Acc"].ToString() == MaHoa.Ma_Hoa(DTB_Acc.Acc))
                {
                    eCongViec e = new eCongViec(con);

                    e.ID = Convert.ToInt32(MaHoa.Giai_Ma(table.Rows[i]["ID"].ToString()));
                    e.XepHang = Convert.ToInt32(MaHoa.Giai_Ma(table.Rows[i]["XepHang"].ToString()));
                    e.LapLai = MaHoa.Giai_Ma(table.Rows[i]["LapLai"].ToString());
                    e.Acc = MaHoa.Giai_Ma(table.Rows[i]["Acc"].ToString());

                    e.NgayTao =Convert.ToDateTime( MaHoa.Giai_Ma(table.Rows[i]["NgayTao"].ToString()));
                    e.TieuDe = MaHoa.Giai_Ma(table.Rows[i]["TieuDe"].ToString());
                    e.NoiDung = MaHoa.Giai_Ma(table.Rows[i]["NoiDung"].ToString());
                    e.OpenFile = MaHoa.Giai_Ma(table.Rows[i]["OpenFile"].ToString());
                    e.DongMinNote = MaHoa.Giai_Ma(table.Rows[i]["DongMinNote"].ToString());
                    e.TatMay = MaHoa.Giai_Ma(table.Rows[i]["TatMay"].ToString());
                    e.LastDay = Convert.ToDateTime(MaHoa.Giai_Ma(table.Rows[i]["LastDay"].ToString()));
                    e.DateTime = Convert.ToDateTime(MaHoa.Giai_Ma(table.Rows[i]["DateTime"].ToString()));
                    if (e.ID > maxID)
                        maxID = e.ID;
                    Data.Add(e);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Update()
        {
            foreach (eCongViec e in Data)
            {
                
                    DateTime N = DateTime.Now,
                             D = e.DateTime;
                    //khong lap............................
                    if (e.LapLai == "Chỉ 1 lần")
                    {
                        if ((D - N).TotalMinutes <= 60 & (D - N).TotalMinutes >= 0)
                        {
                            e.Lua_Chon = true;
                        }
                        else
                            if ((D - N).TotalSeconds < 0 & (e.DateTime - e.LastDay).TotalSeconds > 0)
                            {
                                e.Lua_Chon = true;
                                e.Cham_Che = true;
                                e.LastDay = e.DateTime;
                            }
                            else
                                e.Lua_Chon = false;
                    }
                    //Ngay..............................
                    if (e.LapLai == "Hằng ngày")
                    {
                        DateTime F = new DateTime(N.Year, N.Month, N.Day, D.Hour, D.Minute, 0);
                        if ((F - N).TotalHours <= 2 & (F - N).TotalMinutes >= 0)
                        {
                            e.Lua_Chon = true;
                        }
                        else
                            if ((F - e.LastDay).TotalHours > 24)
                            {
                                e.Lua_Chon = true;
                                e.Cham_Che = true;
                                e.LastDay = F;
                            }
                            else
                                e.Lua_Chon = false;
                    }
                    //Tuan.............................
                    if (e.LapLai == "Hằng tuần")
                    {
                        DateTime F = new DateTime(N.Year, N.Month, N.Day, D.Hour, D.Minute, 0);
                        if (D.DayOfWeek == N.DayOfWeek & ((F - N).TotalHours <= 2 & (F - N).TotalMinutes >= 0))
                        {
                            e.Lua_Chon = true;
                        }
                        else
                            if ((F - e.LastDay).TotalDays > 7)
                            {
                                double g = (F - e.LastDay).TotalDays % 7;
                                e.LastDay = F - new TimeSpan((long)(g * 86400 * 10000000));
                                e.Lua_Chon = true;
                                e.Cham_Che = true;
                            }
                            else
                                e.Lua_Chon = false;
                    }
                    //Thang..............................
                    if (e.LapLai == "Hằng tháng")
                    {
                        DateTime F = new DateTime(N.Year, N.Month, N.Day, D.Hour, D.Minute, 0);
                        DateTime G = new DateTime();
                        bool b = true;
                        try
                        {
                            G = new DateTime(N.Year, N.Month, D.Day, D.Hour, D.Minute, 0, 0);
                        }
                        catch (Exception ex)
                        {
                            b = false;
                        }
                        if (b)
                        {
                            if (D.Day == N.Day & ((F - N).TotalHours <= 2 & (F - N).TotalMinutes >= 0))
                            {
                                e.Lua_Chon = true;
                            }
                            else
                                if (F.DayOfYear > G.DayOfYear & (G - e.LastDay).TotalMinutes > 0)
                                {
                                    e.LastDay = G;
                                    e.Lua_Chon = true;
                                    e.Cham_Che = true;
                                }
                                else
                                    e.Lua_Chon = false;
                        }
                    }

                    //Nam..............................
                    if (e.LapLai == "Hằng năm")
                    {
                        DateTime F = new DateTime(N.Year, N.Month, N.Day, D.Hour, D.Minute, 0);
                        DateTime G = new DateTime();
                        bool b = true;
                        try
                        {
                            G = new DateTime(N.Year, D.Month, D.Day, D.Hour, D.Minute, 0, 0);
                        }
                        catch (Exception ex)
                        {
                            b = false;
                        }
                        if (b)
                        {
                            if (D.DayOfYear == N.DayOfYear & ((F - N).TotalHours <= 2 & (F - N).TotalHours >= 0))
                            {
                                e.Lua_Chon = true;
                            }
                            else
                                if (N.DayOfYear > G.DayOfYear & (G - e.LastDay).TotalMinutes > 0)
                                {
                                    e.LastDay = G;
                                    e.Lua_Chon = true;
                                    e.Cham_Che = true;
                                }
                                else
                                    e.Lua_Chon = false;
                        }
                    }

            }
        }
        public ArrayList GetCongViec()
        {
            ArrayList Get = new ArrayList();
            for (int i = 0; i < Data.Count;i++ )
            {
                if (((eCongViec)Data[i]).Lua_Chon)
                {
                    eCongViec e = (eCongViec)Data[i];
                    DateTime N = DateTime.Now.AddMilliseconds(0);
                    if (!e.Cham_Che)
                    {
                        DateTime D = new DateTime(N.Year, N.Month, N.Day, e.DateTime.Hour, e.DateTime.Minute, 0);
                        if ((D - N).TotalSeconds < 0)
                        {
                            Get.Add(e);
                            e.Lua_Chon = false;
                            e.LastDay = D;
                            con.Command("UPDATE CongViec Set LastDay = '" + MaHoa.Ma_Hoa(D.ToString()) + "'" +
                                                         "WHERE ID = '" + MaHoa.Ma_Hoa(e.ID.ToString()) +
                                                         "' AND Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) + "'");

                        }
                    }
                    else
                    {
                        Get.Add(e);
                        e.Lua_Chon = false;
                        con.Command("UPDATE CongViec Set LastDay = '" + MaHoa.Ma_Hoa(e.LastDay.ToString()) + "'" +
                                                     "WHERE ID = '" + MaHoa.Ma_Hoa(e.ID.ToString()) +
                                                     "' AND Acc = '" + MaHoa.Ma_Hoa(DTB_Acc.Acc) + "'");

                    }
                }
            }
            return Get;
        }
    }
    public class eCongViec : eData
    {
        public DTB_Connect con;
        public eCongViec(DTB_Connect con)
        {
            this.con = con;
        }
        public int UpDate()
        {
            return con.Command("UPDATE CongViec Set XepHang = '" + MaHoa.Ma_Hoa(XepHang.ToString()) + "'," +
                                            "LapLai = '" + MaHoa.Ma_Hoa(LapLai.ToString()) + "'," + 
                                            "TieuDe = '" + MaHoa.Ma_Hoa(TieuDe) + "'," +
                                            "NoiDung = '" + MaHoa.Ma_Hoa(NoiDung) + "'," +
                                            "OpenFile = '" + MaHoa.Ma_Hoa(OpenFile) + "'," +
                                            "DongMinNote = '" + MaHoa.Ma_Hoa(DongMinNote) + "'," + 
                                            "TatMay = '" + MaHoa.Ma_Hoa(TatMay) + "'," +
                                            "NgayTao = '" + MaHoa.Ma_Hoa(NgayTao.ToString()) + "'," +
                                            "LastDay = '" + MaHoa.Ma_Hoa(LastDay.ToString()) + "'," +
                                            "DateTime = '" + MaHoa.Ma_Hoa(DateTime.ToString()) + "'" +
                                            "Where ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                " Acc = '" + MaHoa.Ma_Hoa(Acc) +"'");
        }
        public int InSert()
        {
            return con.Command("INSERT INTO CongViec VALUES ('" + MaHoa.Ma_Hoa(ID.ToString())+ "'" +
                                                        ",'" + MaHoa.Ma_Hoa(Acc) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(NgayTao.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(TieuDe) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(XepHang.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(NoiDung) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(DateTime.ToString()) + "'" +
                                                        ",'" +MaHoa.Ma_Hoa(LapLai.ToString()) +"'" +
                                                        ",'" + MaHoa.Ma_Hoa(OpenFile) + "'"+
                                                        ",'" +MaHoa.Ma_Hoa(DongMinNote) + "'" +
                                                        ",'" +MaHoa.Ma_Hoa(TatMay) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(LastDay.ToString()) +"')");
        }
        public int Delete()
        {
            return con.Command("DELETE FROM CongViec WHERE ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                        "Acc = '" + MaHoa.Ma_Hoa(Acc) + "'");
        }

        public ArrayList ToText()
        {
            ArrayList A = new ArrayList(3);
            A.Add("Tiêu đề: " + TieuDe);
            A.Add("Thời gian: " + DateTime.ToString());
            return A;
        }
        int X, Y;
        public int ID
        {
            set
            {
                X = value;
            }
            get
            {
                return X;
            }
        }
        public int BaseID
        {
            set
            {
                Y = value;
            }
            get
            {
                return Y;
            }
        }
        public int XepHang;
        public String TieuDe, NoiDung, OpenFile, DongMinNote, TatMay, Acc, LapLai;
        public DateTime NgayTao, LastDay, DateTime;
        public bool Lua_Chon = false, Cham_Che = false;
    }
}
