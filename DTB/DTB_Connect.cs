﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;
namespace DTB
{
    public class DTB_Connect
    {
        public SQLiteConnection con;
        public SQLiteCommand Cmd;
        public DTB_Connect()
        {
            con = new SQLiteConnection("URI=file:" + Application.StartupPath + "/DTB.db");
            Cmd = new SQLiteCommand(con);
            con.Open();
        }
        public void Close()
        {
            con.Close();
        }
        public int Command(String S)
        {
            Cmd.CommandText = S;
            return Cmd.ExecuteNonQuery();
        }
    }
}
