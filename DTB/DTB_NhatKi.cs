﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using System.Collections;
namespace DTB
{
    public class DTB_NhatKi
    {
        DataTable table;
        public ArrayList Data;
        public int maxID = 0;
        public DTB_Connect con;
        public DTB_NhatKi(DTB_Connect con)
        {
            table = new DataTable();
            Data = new ArrayList();
            this.con = con;
            SetupTable();
            Loat(con.con);
            Fill();
        }

        void SetupTable()
        {
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ID";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Acc";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NgayTao";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CamThay";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "NoiDung";
            table.Columns.Add(column);
        }
        void Loat(SQLiteConnection con)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM NhatKi", con))
            {

                using (new SQLiteCommandBuilder(da))
                {
                    da.Fill(table);
                }
            }
        }
        void Fill()
        {
            for (int i = table.Rows.Count - 1; i >= 0; i--)
            {
                if(table.Rows[i]["Acc"].ToString() ==MaHoa.Ma_Hoa( DTB_Acc.Acc))
                {
                    eNhatKi e = new eNhatKi(con);
                    e.ID =Convert.ToInt32( MaHoa.Giai_Ma(table.Rows[i]["ID"].ToString()));
                    e.NgayTao = Convert.ToDateTime(MaHoa.Giai_Ma(table.Rows[i]["NgayTao"].ToString()));
                    e.CamThay = MaHoa.Giai_Ma(table.Rows[i]["CamThay"].ToString());
                    e.Acc = MaHoa.Giai_Ma(table.Rows[i]["Acc"].ToString());
                    e.NoiDung = MaHoa.Giai_Ma(table.Rows[i]["NoiDung"].ToString());
                    if (e.ID > maxID)
                        maxID = e.ID;
                    Data.Add(e);
                }
            }
            maxID = Data.Count;
        }
    }
    public class eNhatKi : eData
    {
        public DTB_Connect con;
        public eNhatKi(DTB_Connect con)
        {
            this.con = con;
        }


        public String CamThay, NoiDung, Acc;
        public DateTime NgayTao;
        int X, Y;
        public int ID
        {
            set
            {
                X = value;
            }
            get
            {
                return X;
            }
        }
        public int BaseID
        {
            set
            {
                Y = value;
            }
            get
            {
                return Y;
            }
        }

        public int InSert()
        {
            return con.Command("INSERT INTO NhatKi VALUES ('" + MaHoa.Ma_Hoa(ID.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(Acc) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(NgayTao.ToString()) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(CamThay) + "'" +
                                                        ",'" + MaHoa.Ma_Hoa(NoiDung) + "')");
        }
        public int  UpDate()
        {
            return con.Command("UPDATE NhatKi Set NgayTao = '" + MaHoa.Ma_Hoa(NgayTao.ToString()) + "'," +
                                            "CamThay = '" + MaHoa.Ma_Hoa(CamThay) + "'," +
                                            "NoiDung = '" + MaHoa.Ma_Hoa(NoiDung) + "'" +
                                            "Where ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                " Acc = '" + MaHoa.Ma_Hoa(Acc) + "'");
        }
        public int Delete()
        {
            return con.Command("DELETE FROM NhatKi WHERE ID = '" + MaHoa.Ma_Hoa(ID.ToString()) + "' and " +
                                                        "Acc = '" + MaHoa.Ma_Hoa(Acc) + "'");
        }
        public ArrayList ToText()
        {
            ArrayList A = new ArrayList(3);
            A.Add(" Nhật ký ngày: " + NgayTao.Day.ToString() + 
                                "/" + NgayTao.Month.ToString() + 
                                "/" + NgayTao.Year.ToString());
            A.Add(" Cảm thấy: " +CamThay);
            return A;
        }
    }
}
