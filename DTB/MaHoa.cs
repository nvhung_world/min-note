﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTB
{
    public static class MaHoa
    {
        static int[] Key = {109,7067,1469,43589,8950,518,23467,4851,5818,45671,11110,15465,54561,37895,12366,4895,97812,44569,94120,3411,63454,43};
        
        //..................................................
        public static String Ma_Hoa(String S)
        {
            String Ban_Ma = null;
            if (S !=null)
            {
                Ban_Ma = "";
                for (int i = 0 ; i < S.Length ; i++ )
                {
                    int j = i % Key.Length;
                    if (j == 0) j = Key.Length - 1;
                    int k = ToNumber(S[i]) + Key[j];
                    if (k !=0 & k!=65535)
                        k = k % 65535;
                    Ban_Ma+=ToString(k);
                }
            }
            return Ban_Ma;
        }
        public static String Giai_Ma(String S)
        {
            String Ban_Ro = null;
            if (S != null)
            {
                Ban_Ro = "";
                for (int i = 0; i < S.Length; i++)
                {
                    int j = i % Key.Length;
                    if (j == 0) j = Key.Length - 1;
                    int k = ToNumber(S[i]) + 65535 - Key[j];
                    if (k !=0 & k!=65535)
                        k = k % 65535;
                    Ban_Ro += ToString(k);
                }
            }
            return Ban_Ro;
        }
        //.................................................
        public static int ToNumber(char s)
        {
            return Convert.ToInt32(s);
        }
        public static String ToString(int i)
        {
            return Convert.ToChar(i).ToString();
        }


    }
}
