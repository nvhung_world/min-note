﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace DTB
{
    public interface eData
    {
        int ID
        {
            set;
            get;
        }
        int BaseID
        {
            set;
            get;
        }
        int UpDate();
        int InSert();
        int Delete();
        ArrayList ToText();
    }
}
