﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;
namespace DTB
{
    public class DTB_CaiDatDangNhap
    {
        public DataTable table = new DataTable();
        SQLiteConnection con;
        SQLiteCommand Cmd;
        public static DTB_CaiDatDangNhap C;
        public DTB_CaiDatDangNhap()
        {
            con = new SQLiteConnection("URI=file:" + Application.StartupPath + "/DTB.db");
            Cmd = new SQLiteCommand(con);
            //.................................................

            DataColumn column;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Name";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "GiaTri";
            table.Columns.Add(column);
            C = this;
        }
        public void Open()
        {
            if (con != null)
            {
                con.Open();
                using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM CaiDatDangNhap", con))
                {

                    using (new SQLiteCommandBuilder(da))
                    {
                        da.Fill(table);
                    }
                }
            }
        }
        public void Close()
        {
            if (con != null)
            {
                con.Close();
            }
        }
        
        //............................................
        public bool getGhiNho()
        {
            if (table.Rows[0]["GiaTri"].ToString() == MaHoa.Ma_Hoa("True"))
                return true;
            else
                return false;
        }
        public void setGhiNho(bool B)
        {
            Cmd.CommandText = "UPDATE CaiDatDangNhap Set giaTri = '" + MaHoa.Ma_Hoa(B.ToString()) + "' where Name = 'GhiNho'";
            Cmd.ExecuteNonQuery();
            table.Rows[0]["GiaTri"] = MaHoa.Ma_Hoa(B.ToString());
        }
        //............................................
        public String getAcc()
        {
            if (table.Rows[1]["GiaTri"] != null)
                return MaHoa.Giai_Ma(table.Rows[1]["GiaTri"].ToString());
            else
                return null;
        }
        public void setAcc(String S)
        {
            table.Rows[1]["GiaTri"] = MaHoa.Ma_Hoa(S);
            Cmd.CommandText = "UPDATE CaiDatDangNhap Set giaTri = '" + MaHoa.Ma_Hoa(S) + "' where Name = 'Acc'";
            Cmd.ExecuteNonQuery();
        }
        //............................................
        public String getPass()
        {
            if (table.Rows[2]["GiaTri"] != null)
                return MaHoa.Giai_Ma(table.Rows[2]["GiaTri"].ToString());
            else
                return null;
        }
        public void setPass(String S)
        {
            table.Rows[2]["GiaTri"] = MaHoa.Ma_Hoa(S);
            Cmd.CommandText = "UPDATE CaiDatDangNhap Set giaTri = '" + MaHoa.Ma_Hoa(S) + "' where Name = 'Pass'";
            Cmd.ExecuteNonQuery();
        }
    }
}
